//
//  VoiceCommandDetector.swift
//  proxamamanew
//
//  Created by Nikita Luzhbin on 28.02.2022.
//

import Foundation
import Speech

final class VoiceCommandDetector {

  // MARK: - Instance Properties
  
  private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en_US"))
  private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
  private var recognitionTask: SFSpeechRecognitionTask?
  private let audioEngine = AVAudioEngine()

  // MARK: -

  var onCommandDetected: ((VoiceCommand) -> Void)?

  // MARK: - Instance Methods
      
  func start() {
      SFSpeechRecognizer.requestAuthorization { status in
        switch status {
        case .authorized:
          self.startRecognition()

        default:
          break
        }
      }
  }

  // MARK: -
  
  private func startRecognition() {
      do {
          recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
          guard let recognitionRequest = recognitionRequest else { return }
          
          recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest) { result, error in
            if let result = result,
               let lastCommand = result.bestTranscription.formattedString.lowercased().components(separatedBy: " ").last,
                  let voiceCommand = VoiceCommand(String(lastCommand)) {
              self.onCommandDetected?(voiceCommand)
            }
          }
          
          let recordingFormat = audioEngine.inputNode.outputFormat(forBus: 0)
          audioEngine.inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
              recognitionRequest.append(buffer)
          }
          
          audioEngine.prepare()
          try audioEngine.start()
      }
      
      catch let error {
        print(error.localizedDescription)
      }
  }
}
