//
//  VoiceCommand.swift
//  proxamamanew
//
//  Created by Nikita Luzhbin on 28.02.2022.
//

import Foundation

enum VoiceCommand {

  // MARK: - Enumeration cases

  case start
  case stop

  // MARK: - Initialize

  init?(_ commandString: String) {
    switch commandString {
    case "start":
      self = .start

    case "stop":
      self = .stop

    default:
      return nil
    }
  }
}
