//
//  PMObjectDetectionViewController.swift
//  
//
//  Created by Ankur Pachauri on 19/07/21.
//

import UIKit
import Vision
import AVFoundation

class PMObjectDetectionViewController: ScanningViewController {
  
  var rootLayer: CALayer {
    return _metalLayer
  }
  
  private var detectionOverlay: CALayer! = nil
//  var capturedImageView: UIImageView!
  var orientations: [CGImagePropertyOrientation] = [.up, .upMirrored, .down, .downMirrored, .left, .leftMirrored, .right, .rightMirrored]
  var orientationIndex = 0
  
  // Vision parts
  private var requests = [VNRequest]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupAVCapture()
    
    self.drawVisionRequestResults([])
    
//    capturedImageView = UIImageView(frame: CGRect(x: 60, y: 120, width: 300, height: 500))
//    capturedImageView.backgroundColor = .orange
//    capturedImageView.contentMode = .scaleAspectFit
//    view.addSubview(capturedImageView)
  }
  
  @discardableResult
  func setupVision() -> NSError? {
    // Setup Vision parts
    let error: NSError! = nil
    
    guard let modelURL = Bundle.main.url(forResource: "ObjectDetector", withExtension: "mlmodelc") else {
      return NSError(domain: "VisionObjectRecognitionViewController", code: -1, userInfo: [NSLocalizedDescriptionKey: "Model file is missing"])
    }
    do {
      let visionModel = try VNCoreMLModel(for: MLModel(contentsOf: modelURL))
      let objectRecognition = VNCoreMLRequest(model: visionModel, completionHandler: { (request, error) in
        
        print("objectRecognition request completion:")
        DispatchQueue.main.async(execute: {
          // perform all the UI updates on the main queue
          if let results = request.results {
            self.drawVisionRequestResults(results)
          }
        })
      })
      self.requests = [objectRecognition]
    } catch let error as NSError {
      print("Model loading went wrong: \(error)")
    }
    
    return error
  }
  
  func drawVisionRequestResults(_ results: [Any]) {
    
    print("drawVisionRequestResults: \(results.count)")
    CATransaction.begin()
    CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
    detectionOverlay.sublayers = nil // remove all the old recognized objects
    for observation in results where observation is VNRecognizedObjectObservation {
      guard let objectObservation = observation as? VNRecognizedObjectObservation else {
        continue
      }
      // Select only the label with the highest confidence.
      let topLabelObservation = objectObservation.labels[0]
      var objectBounds = VNImageRectForNormalizedRect(objectObservation.boundingBox, Int(bufferSize.width), Int(bufferSize.height))

      print("objectObservation.boundingBox: \(objectObservation.boundingBox)")
      print("objectBounds: \(objectBounds)")
//      objectBounds = CGRect(x: 0, y: 0, width: objectBounds.width, height: objectBounds.height)
      let shapeLayer = self.createRoundedRectLayerWithBounds(objectBounds)

      let textLayer = self.createTextSubLayerInBounds(objectBounds,
                                                      identifier: topLabelObservation.identifier,
                                                      confidence: topLabelObservation.confidence)
      shapeLayer.addSublayer(textLayer)
      detectionOverlay.addSublayer(shapeLayer)
    }
    
//    let bounds = CGRect(x: 0, y: 0, width: 200, height: 200)
//
//    let shapeLayer = self.createRoundedRectLayerWithBounds(bounds)
//
//    let textLayer = self.createTextSubLayerInBounds(bounds,
//                                                    identifier: "Test",
//                                                    confidence: 0.99)
//    shapeLayer.addSublayer(textLayer)
//    detectionOverlay.addSublayer(shapeLayer)

    
    self.updateLayerGeometry()
    CATransaction.commit()
  }
  
  override func cameraDidOutput(colorBuffer: CVPixelBuffer, depthBuffer: CVPixelBuffer, depthCalibrationData: AVCameraCalibrationData) {
    
//    let ciimage = CIImage(cvPixelBuffer: colorBuffer) // depth cvPixelBuffer
//    let depthUIImage = UIImage(ciImage: ciimage)
//    DispatchQueue.main.async {
//      self.capturedImageView.image = depthUIImage
//    }
        
    super.cameraDidOutput(colorBuffer: colorBuffer, depthBuffer: depthBuffer, depthCalibrationData: depthCalibrationData)
    
    let exifOrientation = exifOrientationFromDeviceOrientation()
    
//    let index = orientationIndex % orientations.count
//    let orientation = orientations[index]
//    orientationIndex += 1
    
    let imageRequestHandler = VNImageRequestHandler(cvPixelBuffer: colorBuffer, options: [:])
        do {
          try imageRequestHandler.perform(self.requests)
        } catch {
          print("imageRequestHandler: \(error)")
        }
  }
  
  func setupAVCapture() {
    
    // setup Vision parts
    setupLayers()
    updateLayerGeometry()
    setupVision()
  }
  
  func setupLayers() {
    detectionOverlay = CALayer() // container layer that has all the renderings of the observations
    detectionOverlay.name = "DetectionOverlay"
    detectionOverlay.bounds = CGRect(x: 0.0,
                                     y: 0.0,
                                     width: bufferSize.width,
                                     height: bufferSize.height)
    detectionOverlay.position = CGPoint(x: rootLayer.bounds.midX, y: rootLayer.bounds.midY)
    rootLayer.addSublayer(detectionOverlay)
  }
  
  func updateLayerGeometry() {
    let bounds = rootLayer.bounds
    var scale: CGFloat
    
    let xScale: CGFloat = bounds.size.width / bufferSize.height
    let yScale: CGFloat = bounds.size.height / bufferSize.width
    
    scale = fmax(xScale, yScale)
    if scale.isInfinite {
      scale = 1.0
    }
    CATransaction.begin()
    CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
    
    // rotate the layer into screen orientation and scale and mirror
    detectionOverlay.setAffineTransform(CGAffineTransform(scaleX: scale, y: -scale))    
    
    // center the layer
    detectionOverlay.position = CGPoint(x: bounds.midX, y: bounds.midY)
    
    CATransaction.commit()
    
  }
  
  func createTextSubLayerInBounds(_ bounds: CGRect, identifier: String, confidence: VNConfidence) -> CATextLayer {
    let textLayer = CATextLayer()
    textLayer.name = "Object Label"
    let formattedString = NSMutableAttributedString(string: String(format: "\(identifier)\nConfidence:  %.2f", confidence))
    let largeFont = UIFont(name: "Helvetica", size: 24.0)!
    formattedString.addAttributes([NSAttributedString.Key.font: largeFont], range: NSRange(location: 0, length: identifier.count))
    textLayer.string = formattedString
    textLayer.bounds = CGRect(x: 0, y: 0, width: bounds.size.height - 10, height: bounds.size.width - 10)
    textLayer.position = CGPoint(x: bounds.midX, y: bounds.midY)
    textLayer.shadowOpacity = 0.7
    textLayer.shadowOffset = CGSize(width: 2, height: 2)
    textLayer.foregroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [0.0, 0.0, 0.0, 1.0])
    textLayer.contentsScale = 2.0 // retina rendering
    // rotate the layer into screen orientation and scale and mirror
    textLayer.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(.pi / 2.0)).scaledBy(x: 1.0, y: -1.0))
    return textLayer
  }
  
  func createRoundedRectLayerWithBounds(_ bounds: CGRect) -> CALayer {
    let shapeLayer = CALayer()
    shapeLayer.bounds = bounds
    shapeLayer.position = CGPoint(x: bounds.midX, y: bounds.midY)
    shapeLayer.name = "Found Object"
    shapeLayer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 0.2, 0.4])
    shapeLayer.cornerRadius = 7
    return shapeLayer
  }
  
  public func exifOrientationFromDeviceOrientation() -> CGImagePropertyOrientation {
    let curDeviceOrientation = UIDevice.current.orientation
    let exifOrientation: CGImagePropertyOrientation
    
    switch curDeviceOrientation {
    case UIDeviceOrientation.portraitUpsideDown:  // Device oriented vertically, home button on the top
      exifOrientation = .left
    case UIDeviceOrientation.landscapeLeft:       // Device oriented horizontally, home button on the right
      exifOrientation = .upMirrored
    case UIDeviceOrientation.landscapeRight:      // Device oriented horizontally, home button on the left
      exifOrientation = .down
    case UIDeviceOrientation.portrait:            // Device oriented vertically, home button on the bottom
      exifOrientation = .up
    default:
      exifOrientation = .up
    }
    return exifOrientation
  }
}
