import UIKit
import AVFoundation
import Vision

class ObjectRecognitionViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    private let captureSession = AVCaptureSession()
    private lazy var previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
    private let videoDataOutput = AVCaptureVideoDataOutput()
    private var drawings: [CAShapeLayer] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addCameraInput()
        self.showCameraFeed()
        self.getCameraFrames()
        self.captureSession.startRunning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.previewLayer.frame = self.view.frame
    }

    override func viewDidDisappear(_ animated: Bool) {
      self.captureSession.stopRunning()
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let frame = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            debugPrint("unable to get image from sample buffer")
            return
        }

        self.detectObject(in: frame)
    }
    
    private func addCameraInput() {
        guard let device = AVCaptureDevice.DiscoverySession(
            deviceTypes: [.builtInWideAngleCamera, .builtInDualCamera, .builtInTrueDepthCamera],
            mediaType: .video,
            position: .front).devices.first else {
                fatalError("No back camera device found, please make sure to run SimpleLaneDetection in an iOS device and not a simulator")
        }
        let cameraInput = try! AVCaptureDeviceInput(device: device)
        self.captureSession.addInput(cameraInput)
    }
    
    private func showCameraFeed() {
        self.previewLayer.videoGravity = .resizeAspectFill
        self.view.layer.addSublayer(self.previewLayer)
        self.previewLayer.frame = self.view.frame
    }
    
    private func getCameraFrames() {
        self.videoDataOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString) : NSNumber(value: kCVPixelFormatType_32BGRA)] as [String : Any]
        self.videoDataOutput.alwaysDiscardsLateVideoFrames = true
        self.videoDataOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "camera_frame_processing_queue"))
        self.captureSession.addOutput(self.videoDataOutput)
        guard let connection = self.videoDataOutput.connection(with: AVMediaType.video),
            connection.isVideoOrientationSupported else { return }
        connection.videoOrientation = .portrait
    }
    
    private func detectObject(in image: CVPixelBuffer) {
      let nippleDetector = try! NippleDetector().model
      let visionModel = try! VNCoreMLModel(for: nippleDetector)
      
      let objectRecognition = VNCoreMLRequest(model: visionModel, completionHandler: { (request, error) in
          DispatchQueue.main.async(execute: {
              if let results = request.results as? [VNRecognizedObjectObservation] {
                self.handleObjectDetectionResults(results)
              } else {
                self.clearDrawings()
              }
          })
      })

      objectRecognition.imageCropAndScaleOption = .scaleFill

      let imageRequestHandler = VNImageRequestHandler(cvPixelBuffer: image, orientation: .leftMirrored, options: [:])
      try? imageRequestHandler.perform([objectRecognition])
    }
    
  private func handleObjectDetectionResults(_ observedObjects: [VNRecognizedObjectObservation]) {
    self.clearDrawings()

    guard
      let filteredObservation = self.filterObservations(observations: observedObjects)
    else {
      return
    }

    let objectBoundingBoxOnScreen = self.previewLayer.layerRectConverted(fromMetadataOutputRect: filteredObservation.boundingBox)
    let objectBoundingBoxPath = CGPath(rect: objectBoundingBoxOnScreen, transform: nil)
    let objectBoundingBoxShape = CAShapeLayer()

    objectBoundingBoxShape.path = objectBoundingBoxPath
    objectBoundingBoxShape.fillColor = UIColor.clear.cgColor
    objectBoundingBoxShape.strokeColor = UIColor.green.cgColor

    self.view.layer.addSublayer(objectBoundingBoxShape)
    self.drawings = [objectBoundingBoxShape]
  }
    
  private func clearDrawings() {
      self.drawings.forEach({ drawing in drawing.removeFromSuperlayer() })
  }

  private func filterObservations(observations: [VNRecognizedObjectObservation]) -> VNRecognizedObjectObservation? {
    var nippleComplexObservation: VNRecognizedObjectObservation?
    var nippleObservation: VNRecognizedObjectObservation?

    for observation in observations {
      guard
        let mainClassificationObservation = observation.labels.max(by: { $0.confidence < $1.confidence })
      else {
        continue
      }

      switch mainClassificationObservation.identifier {
      case "NippleComplex" where observation.confidence > (nippleComplexObservation?.confidence ?? 0.0):
        nippleComplexObservation = observation
        
      case "Nipple" where observation.confidence > (nippleObservation?.confidence ?? 0.0):
        nippleObservation = observation

      default:
        continue
      }
    }

    guard
      let nippleObservation = nippleObservation,
      let nippleComplexObservation = nippleComplexObservation
    else {
      return nil
    }

    let sampleNippleRect = VNNormalizedRectForImageRect(nippleObservation.boundingBox, 100, 100)
    let sampleNippleComplexRect = VNNormalizedRectForImageRect(nippleComplexObservation.boundingBox, 100, 100)

    let nippleBboxCenter = CGPoint(x: sampleNippleRect.midX, y: sampleNippleRect.midY)

    guard
      sampleNippleComplexRect.contains(nippleBboxCenter)
    else {
      return nil
    }

    return nippleComplexObservation
  }
}
