//
//  ShareActivityManager.swift
//  proxaMama
//
//  Created by Ankur Pachauri on 08/07/21.
//

import Foundation
import UIKit

class ShareActivity {
    
    let title: String?
    var url: URL?

    init?(title: String? = nil, url: URL? = nil) {
        
        if title == nil && url == nil {
            return nil
        }
        
        self.title = title
        self.url = url
    }
}

class ShareActivityManager {
    
    static let excludedActivityTypes: [UIActivity.ActivityType] = [.print, .postToWeibo, .postToVimeo, .assignToContact, .addToReadingList, .copyToPasteboard]
    
    static func activityViewController(activity: ShareActivity, excludeTypes: [UIActivity.ActivityType] = excludedActivityTypes) -> UIActivityViewController {
        
        var activityItems = [] as [Any]
        
        if let title = activity.title {
            activityItems.append(title)
        }

        if let url = activity.url {
            activityItems.append(url)
        }
       
        let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = excludeTypes
        
        return activityViewController
    }
}
