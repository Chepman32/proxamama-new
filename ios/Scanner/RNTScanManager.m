//
//  RNTScanManager.m
//  proxaMama
//
//  Created by Ankur Pachauri on 15/06/21.
//

#import "RNTScanManager.h"
#import "proxamamanew-Swift.h"
#import "AppDelegate.h"

@implementation RNTScanManager

RCT_EXPORT_MODULE(RNTScanner);

RCT_EXPORT_METHOD(showScan)
{
  dispatch_async(dispatch_get_main_queue(), ^{
    NSLog(@"showScan called");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UINavigationController *navController = (UINavigationController *)appDelegate.window.rootViewController;
    UIViewController *scanViewController = [ScannerProvider getScannerView];
    [navController pushViewController:scanViewController animated:YES];
  });
}

//- (void)showScan
//{
//
//  NSLog(@"showScan called");
//  AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//
//  UINavigationController *navController = (UINavigationController *)appDelegate.window.rootViewController;
//  UIViewController *scanViewController = [ScannerProvider getScannerView];
//  [navController pushViewController:scanViewController animated:YES];
//}

@end
