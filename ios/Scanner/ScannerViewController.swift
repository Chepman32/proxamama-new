//
//  ScannerViewController.swift
//  proxaMama
//
//  Created by Ankur Pachauri on 15/06/21.
//

import UIKit

class ScannerViewController: UIViewController {
  
  let scannerView = ScannerView(frame: CGRect(x: 0, y: 0, width: 414, height: 560))
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.view.addSubview(scannerView)
    scannerView.startCaptureSession()
  }
}
