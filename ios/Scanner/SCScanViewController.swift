//
//  SCScanViewController.swift
//  proxaMama
//
//  Created by Ankur Pachauri on 05/07/21.
//

import UIKit
import StandardCyborgFusion
import ZIPFoundation
import Vision
import CoreML

class SCScanViewController: UIViewController {
  
  @IBOutlet weak var scanImageView: UIImageView!
  @IBOutlet private weak var showScanButton: UIButton!
  @IBOutlet weak var exportPointCloudButton: UIButton!
  @IBOutlet weak var exportVideoButton: UIButton!
  @IBOutlet weak var exportFramesButton: UIButton!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

  let dispatchGroup = DispatchGroup()

  private var requests = [VNRequest]()
  private var recognizedImages: [(URL, Float)] = [] // <- Image URL and it's ML confidence score
  private let minimumAcceptableConfidence: Float = 0.6
  
  private var lastScene: SCScene?
  private var lastSceneDate: Date?
  private var lastSceneThumbnail: UIImage?
  private var scenePreviewVC: ScenePreviewViewController?
  
  private lazy var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
  private lazy var sceneGltfURL = documentsURL.appendingPathComponent("scene.gltf")
  private lazy var sceneThumbnailURL = documentsURL.appendingPathComponent("scene.png")
  private lazy var pointCloudPlyURL = documentsURL.appendingPathComponent("pointCloud.ply")
  private lazy var movieOutputURL = documentsURL.appendingPathComponent("videoFile_test.mov")
  private lazy var framesDirectoryURL = documentsURL.appendingPathComponent("frames")
  private lazy var framesZipURL = documentsURL.appendingPathComponent("framesArchive.zip")
  
  override var preferredStatusBarStyle: UIStatusBarStyle { .lightContent }
  
  private let dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .medium
    formatter.timeStyle = .short
    return formatter
  }()
  
  // MARK: - Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    scanImageView.contentMode = .scaleAspectFill
    scanImageView.layer.borderColor = UIColor.white.cgColor
    scanImageView.layer.borderWidth = 1.0
    scanImageView.layer.cornerRadius = 5
        
    loadScene()
  }
  
  // MARK: - User Interaction
  @IBAction private func back(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
  }
  
  @IBAction private func startScanning(_ sender: UIButton) {
    print("startScanning tapped")
    #if targetEnvironment(simulator)
    let alert = UIAlertController(title: "Simulator Unsupported", message: "There is no depth camera available on the iOS Simulator. Please build and run on an iOS device with TrueDepth", preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
    present(alert, animated: true)
    #else
    let scanningVC = ScanningViewController()
    scanningVC.delegate = self
    scanningVC.generatesTexturedMeshes = true
    //      self.navigationController?.pushViewController(scanningVC, animated: true)
    scanningVC.modalPresentationStyle = .fullScreen
    present(scanningVC, animated: true)
    #endif
  }

  @IBAction private func liveRecognitionTapped(_ sender: UIButton) {
    let vc = ObjectRecognitionViewController()
    self.present(vc, animated: true, completion: nil)
  }
  
  @IBAction private func showScanButtonTapped(_ sender: UIButton) {
    print("showScan tapped")
    guard let scScene = lastScene else { return }
    
    let vc = ScenePreviewViewController(scScene: scScene)
    vc.leftButton.addTarget(self, action: #selector(deletePreviewedSceneTapped), for: UIControl.Event.touchUpInside)
    vc.rightButton.addTarget(self, action: #selector(dismissPreviewedScanTapped), for: UIControl.Event.touchUpInside)
    vc.leftButton.setTitle("Delete", for: UIControl.State.normal)
    vc.rightButton.setTitle("Dismiss", for: UIControl.State.normal)
    vc.leftButton.backgroundColor = UIColor(named: "DestructiveAction")
    vc.rightButton.backgroundColor = UIColor(named: "DefaultAction")
    vc.modalPresentationStyle = .fullScreen
    scenePreviewVC = vc
    present(vc, animated: true)
  }
  
  @IBAction func exportPointCloudButtonTapped(_ sender: Any) {
        
    guard let _ = lastScene else { return }
    
    share(url: pointCloudPlyURL)
  }
  
  @IBAction func exportVideoButtonTapped(_ sender: Any) {
    
    guard let _ = lastScene else { return }
    
    share(url: movieOutputURL)
  }
  
  @IBAction func exportFramesButtonTapped(_ sender: Any) {
    
    guard let _ = lastScene else { return }
    
    if self.activityIndicator.isAnimating {
      return
    }
    
    let fileManager = FileManager()
    
    if fileManager.fileExists(atPath: framesZipURL.path) {
      try? fileManager.removeItem(at: framesZipURL)
    }
    
    activityIndicator.isHidden = false
    activityIndicator.startAnimating()
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.25, execute: {
      do {
        try fileManager.zipItem(at: self.framesDirectoryURL, to: self.framesZipURL)
        self.activityIndicator.stopAnimating()
        self.share(url: self.framesZipURL)
      } catch {
        print("Creation of ZIP archive failed with error:\(error)")
        self.activityIndicator.stopAnimating()
      }
    })
  }

  @IBAction func exportKeyFrameButtonTapped(_ sender: UIButton) {
    let fileManager = FileManager()

    guard
      let _ = lastScene,
      fileManager.fileExists(atPath: self.framesDirectoryURL.path)
    else {
      return
    }

    DispatchQueue.global(qos: .userInitiated).async {
      do {
        let model = try NippleDetector().model
        let visionModel = try VNCoreMLModel(for: model)

        let directoryContents = try FileManager.default.contentsOfDirectory(at: self.framesDirectoryURL,
                                                                            includingPropertiesForKeys: nil)

        try directoryContents
          .filter({ $0.lastPathComponent.contains("color") })
          .forEach({ imageURL in
            let objectRecognition = VNCoreMLRequest(model: visionModel, completionHandler: { (request, error) in
              self.recognize(imageURL: imageURL, request: request, error: error)
            })

            self.requests = [objectRecognition]
            
            let handler = VNImageRequestHandler(url: imageURL)
            try handler.perform(self.requests)
          })

        guard
          let keyFrameImage = self.recognizedImages
            .filter({ $0.1 > self.minimumAcceptableConfidence })
            .max(by: { $0.1 < $1.1 })
        else {
          print("KeyFrame is not found")
          return
        }

        DispatchQueue.main.async {
          self.share(url: keyFrameImage.0)
        }
      } catch {
        print(error)
      }
    }
  }

  func recognize(imageURL: URL, request: VNRequest, error: Error?) {
    // TODO: - Error handling

    if let results = request.results, let observation = results.first {
      self.recognizedImages.append((imageURL, observation.confidence))
    }
  }
  
  @objc private func deletePreviewedSceneTapped() {
    deleteScene()
    dismiss(animated: true)
  }
  
  @objc private func dismissPreviewedScanTapped() {
    dismiss(animated: false)
  }
  
  @objc private func savePreviewedSceneTapped() {
    saveScene(scene: scenePreviewVC!.scScene, thumbnail: scenePreviewVC?.renderedSceneImage)
    dismiss(animated: true)
  }
  
  // MARK: - Scene I/O
  
  private func loadScene() {
    if
      FileManager.default.fileExists(atPath: sceneGltfURL.path),
      let gltfAttributes = try? FileManager.default.attributesOfItem(atPath: sceneGltfURL.path),
      let dateCreated = gltfAttributes[FileAttributeKey.creationDate] as? Date
    {
      let scene = SCScene(gltfAtPath: sceneGltfURL.path)
      lastScene = scene
      lastSceneDate = dateCreated
      lastSceneThumbnail = UIImage(contentsOfFile: sceneThumbnailURL.path)
    }
    
    updateUI()
  }
  
  private func saveScene(scene: SCScene, thumbnail: UIImage?) {
    
    scene.writeToGLTF(atPath: sceneGltfURL.path)
    
    if let pointCloud = scene.pointCloud {
      savePointCloud(pointCloud: pointCloud)
    }
    
    if let thumbnail = thumbnail, let pngData = thumbnail.pngData() {
      try? pngData.write(to: sceneThumbnailURL)
    }
    
    lastScene = scene
    lastSceneThumbnail = thumbnail
    lastSceneDate = Date()
    
    updateUI()
  }
  
  private func deleteScene() {
    let fileManager = FileManager.default
    
    if fileManager.fileExists(atPath: sceneGltfURL.path) {
      try? fileManager.removeItem(at: sceneGltfURL)
    }
        
    if fileManager.fileExists(atPath: sceneThumbnailURL.path) {
      try? fileManager.removeItem(at: sceneThumbnailURL)
    }
    
    if fileManager.fileExists(atPath: pointCloudPlyURL.path) {
      try? fileManager.removeItem(at: pointCloudPlyURL)
    }

    if fileManager.fileExists(atPath: movieOutputURL.path) {
      try? fileManager.removeItem(at: movieOutputURL)
    }
    
    lastScene = nil
    lastSceneThumbnail = nil
    lastSceneDate = nil
    
    updateUI()
  }
  
  private func savePointCloud(pointCloud: SCPointCloud) {
    
    pointCloud.writeToPLY(atPath: pointCloudPlyURL.path)
    
    share(url: pointCloudPlyURL)
  }
  
  private func share(url:URL) {
    
    guard let shareActivity = ShareActivity(url: url) else {
      return
    }
    
    DispatchQueue.main.async {
      let activityViewController = ShareActivityManager.activityViewController(activity: shareActivity)
      self.present(activityViewController, animated: true, completion: nil)
    }
  }
  
  // MARK: - Helpers
  
  private func updateUI() {
    if lastSceneThumbnail == nil {
      showScanButton.setTitle("no scan yet", for: UIControl.State.normal)
    } else {
      showScanButton.setTitle(nil, for: UIControl.State.normal)
    }
    
    scanImageView.image = lastSceneThumbnail
  }
}

extension SCScanViewController: ScanningViewControllerDelegate {
  
  func scanningViewControllerDidCancel(_ controller: ScanningViewController) {
    dismiss(animated: true)
  }
  
  func scanningViewController(_ controller: ScanningViewController, didScan pointCloud: SCPointCloud) {
    let vc = ScenePreviewViewController(pointCloud: pointCloud, meshTexturing: controller.meshTexturing, landmarks: nil)
    vc.leftButton.addTarget(self, action: #selector(dismissPreviewedScanTapped), for: UIControl.Event.touchUpInside)
    vc.rightButton.addTarget(self, action: #selector(savePreviewedSceneTapped), for: UIControl.Event.touchUpInside)
    vc.leftButton.setTitle("Rescan", for: UIControl.State.normal)
    vc.rightButton.setTitle("Save", for: UIControl.State.normal)
    vc.leftButton.backgroundColor = UIColor(named: "DestructiveAction")
    vc.rightButton.backgroundColor = UIColor(named: "SaveAction")
    scenePreviewVC = vc
    controller.present(vc, animated: false)
  }
}

private extension URL {
  static let documentsURL: URL = {
    guard let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, false).first
    else { fatalError("Failed to find the documents directory") }
    
    // Annoyingly, this gives us the directory path with a ~ in it, so we have to expand it
    let tildeExpandedDocumentsDirectory = (documentsDirectory as NSString).expandingTildeInPath
    
    return URL(fileURLWithPath: tildeExpandedDocumentsDirectory)
  }()
}

