/**
 * @format
 */

import { AppRegistry } from 'react-native';

import { name as appName } from './app.json';
import { App } from './src';
import './reactotron.ts';

AppRegistry.registerComponent(appName, () => App);
