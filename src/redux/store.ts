import { init } from '@rematch/core';
import loadingPlugin from '@rematch/loading';
import { StoreEnhancer } from 'redux';

import { reactotronInstance } from '../../reactotron';

import { FullModel, models, RootModel } from './models';

const enhancers: StoreEnhancer[] = [];

if (__DEV__ && reactotronInstance.createEnhancer) {
  enhancers.push(reactotronInstance.createEnhancer());
}

export const store = init<RootModel, FullModel>({
  models,
  plugins: [loadingPlugin()],
  redux: {
    enhancers,
  },
});
