import { Models } from '@rematch/core';
import { ExtraModelsFromLoading } from '@rematch/loading';

import { authFlow } from './authFlow';

export interface RootModel extends Models<RootModel> {
  authFlow: typeof authFlow;
}
export type FullModel = ExtraModelsFromLoading<RootModel>;

export const models: RootModel = { authFlow };
