import SplashScreen from 'react-native-splash-screen';

import AsyncStorage from '@react-native-community/async-storage';
import { createModel } from '@rematch/core';
import delay from 'delay';

import { RootModel } from '..';

interface AuthFlowState {
  userToken?: string | null;
}

const AUTH_FLOW_DELAY_MS = 2000;
const AUTH_TOKEN_KEY =
  '3C469E9D6C5875D37A43F353D4F88E61FCF812C66EEE3457465A40B0DA4153E0';

export const authFlow = createModel<RootModel>()({
  state: {
    userToken: null,
    isLoading: false,
    isSignout: false,
  } as AuthFlowState,
  reducers: {
    setToken(state, token: string | null) {
      return {
        ...state,
        userToken: token,
      };
    },
  },
  effects: dispatch => ({
    signin: async (token: string) => {
      await AsyncStorage.setItem(AUTH_TOKEN_KEY, token);
      dispatch.authFlow.setToken(token);
    },
    checkToken: async () => {
      try {
        const token = await AsyncStorage.getItem(AUTH_TOKEN_KEY);
        if (token) {
          dispatch.authFlow.setToken(token);
        }
        await delay(AUTH_FLOW_DELAY_MS);
      } finally {
        SplashScreen.hide();
      }
    },
    signout: async () => {
      await AsyncStorage.removeItem(AUTH_TOKEN_KEY);
      dispatch.authFlow.setToken(null);
    },
  }),
});
