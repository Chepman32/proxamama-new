export const MutationKeys = {
  GOOGLE_AUTH: 'googleAuth',
  FACEBOOK_AUTH: 'facebookAuth',
  APPLE_AUTH: 'appleAuth',
};
