import {
  ExtractRematchDispatchersFromModels,
  RematchRootState,
} from '@rematch/core';
import { FullModel, RootModel } from 'redux/models';

declare module 'redux' {
  export interface Dispatch
    extends ExtractRematchDispatchersFromModels<RootModel> {}
}

declare module 'react-redux' {
  export interface DefaultRootState
    extends RematchRootState<RootModel, FullModel> {}
}
