import '@emotion/react';

import { theme } from '~/config';

declare module '@emotion/react' {
  type Palette = keyof typeof theme.palette;
  export interface Theme {
    palette: {
      // eslint-disable-next-line no-unused-vars
      [key in Palette]: string;
    };
  }
}
