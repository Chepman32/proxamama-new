import { NavigatorScreenParams } from '@react-navigation/native';

declare global {
  type AuthNavigatorParamList = {
    Intro: undefined;
    Login: undefined;
    Signup: undefined;
  };

  type DrawerNavigatorParamList = {
    TabNavigator: NavigatorScreenParams<TabNavigatorParamList>;
    BottleDetails: undefined;
  };

  type TabNavigatorParamList = {
    Baby: undefined;
    ShopStackNavigator: NavigatorScreenParams<ShopStackParamList>;
    Support: undefined;
  };

  type ShopStackParamList = {
    Shop: undefined;
    Bottles: undefined;
    Addons: undefined;
    FullSystem: undefined;
    BottleDetails: undefined;
    AccessoriesDetails: undefined;
  };

  namespace ReactNavigation {
    interface RootParamList
      extends AuthNavigatorParamList,
        TabNavigatorParamList,
        ShopStackParamList {}
  }
}

export {};
