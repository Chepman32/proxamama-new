import React from 'react';
import { initialWindowMetrics, Metrics } from 'react-native-safe-area-context';

import { createStackNavigator } from '@react-navigation/stack';

import { Back } from '~/atoms/common';
import { theme } from '~/config';
import { IntroScreen, LoginScreen, SignupScreen } from '~/screens';

const AuthStack = createStackNavigator<AuthNavigatorParamList>();

export const AuthNavigator = () => {
  return (
    <AuthStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: theme.palette.placeboOrange,
          shadowOffset: {
            width: 0,
            height: 0,
          },
          height: (initialWindowMetrics as Metrics).insets.top + 70,
        },
        headerLeft: ({ canGoBack }) => (canGoBack ? <Back /> : null),
        headerLeftContainerStyle: {
          paddingLeft: 24,
        },
        headerRightContainerStyle: {
          paddingRight: 24,
        },
        title: '',
      }}
    >
      <AuthStack.Screen
        name="Intro"
        options={{ headerShown: false }}
        component={IntroScreen}
      />
      <AuthStack.Screen name="Login" component={LoginScreen} />
      <AuthStack.Screen name="Signup" component={SignupScreen} />
    </AuthStack.Navigator>
  );
};
