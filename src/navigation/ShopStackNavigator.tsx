import React, { FC } from 'react';
import { initialWindowMetrics, Metrics } from 'react-native-safe-area-context';

import { DrawerActions, useNavigation } from '@react-navigation/native';
import {
  createStackNavigator,
  StackNavigationOptions,
} from '@react-navigation/stack';

import { Avatar } from '~/atoms/auth';
import { CartButton } from '~/atoms/cart';
import { Back } from '~/atoms/common';
import { theme } from '~/config';
import {
  AccesoriesDetailsScreen,
  AddonsScreen,
  BottleDetailsScreen,
  BottlesScreen,
  FullSystemScreen,
  ShopScreen,
} from '~/screens';

const StackNavigator = createStackNavigator<ShopStackParamList>();

const baseHeaderStyle = {
  backgroundColor: theme.palette.placeboOrange,
  shadowOffset: {
    width: 0,
    height: 0,
  },
  height: (initialWindowMetrics as Metrics).insets.top + 70,
};

const categoriesHeaderOptions: StackNavigationOptions = {
  headerStyle: {
    ...baseHeaderStyle,
    backgroundColor: theme.palette.white,
  },
};

export const ShopStackNavigator: FC = () => {
  const navigation = useNavigation();
  return (
    <StackNavigator.Navigator
      screenOptions={{
        headerStyle: baseHeaderStyle,
        headerLeft: ({ canGoBack }) =>
          canGoBack ? (
            <Back />
          ) : (
            <Avatar
              onPress={() => navigation.dispatch(DrawerActions.openDrawer())}
            />
          ),
        headerLeftContainerStyle: {
          paddingLeft: 24,
        },
        headerRightContainerStyle: {
          paddingRight: 24,
        },
        title: '',
        headerRight: () => <CartButton />,
      }}
    >
      <StackNavigator.Screen name="Shop" component={ShopScreen} />
      <StackNavigator.Screen
        name="Bottles"
        component={BottlesScreen}
        options={categoriesHeaderOptions}
      />
      <StackNavigator.Screen
        name="Addons"
        component={AddonsScreen}
        options={categoriesHeaderOptions}
      />
      <StackNavigator.Screen name="FullSystem" component={FullSystemScreen} />
      <StackNavigator.Screen
        name="AccessoriesDetails"
        component={AccesoriesDetailsScreen}
      />
      <StackNavigator.Screen
        name="BottleDetails"
        component={BottleDetailsScreen}
      />
    </StackNavigator.Navigator>
  );
};
