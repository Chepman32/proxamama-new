import React, { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { BottomSheetModalProvider } from '@gorhom/bottom-sheet';
import { NavigationContainer } from '@react-navigation/native';
import {} from '@rematch/loading';

import { AuthNavigator } from './AuthNavigator';
import { DrawerNavigator } from './DrawerNavigator';

import { navigationRef } from './index';

export const RootNavigator: FC = () => {
  const userToken = useSelector(state => state.authFlow.userToken);
  const { authFlow } = useDispatch();
  useEffect(() => {
    authFlow.checkToken();
  }, [authFlow]);
  return (
    <BottomSheetModalProvider>
      <NavigationContainer ref={navigationRef}>
        {userToken ? <DrawerNavigator /> : <AuthNavigator />}
      </NavigationContainer>
    </BottomSheetModalProvider>
  );
};
