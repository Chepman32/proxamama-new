import React, { FC } from 'react';

import { createDrawerNavigator } from '@react-navigation/drawer';

import { theme } from '~/config';
import { DrawerMenu } from '~/organisms/navigation';

import { TabNavigator } from './TabNavigator';

const Drawer = createDrawerNavigator<DrawerNavigatorParamList>();

export const DrawerNavigator: FC = () => {
  return (
    <Drawer.Navigator
      screenOptions={{
        headerShown: false,
        drawerStyle: {
          width: '100%',
          backgroundColor: theme.palette.greekAubergine,
        },
      }}
      drawerContent={props => <DrawerMenu {...props} />}
    >
      <Drawer.Screen name="TabNavigator" component={TabNavigator} />
    </Drawer.Navigator>
  );
};
