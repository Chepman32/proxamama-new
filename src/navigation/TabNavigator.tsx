import React, { FC } from 'react';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { TabBar } from '~/organisms/navigation';
import { SupportScreen } from '~/screens';

import { ShopStackNavigator } from './ShopStackNavigator';

const Tab = createBottomTabNavigator<TabNavigatorParamList>();

export const TabNavigator: FC = () => {
  return (
    <Tab.Navigator
      screenOptions={{ headerShown: false }}
      initialRouteName="ShopStackNavigator"
      tabBar={TabBar}
    >
      <Tab.Screen name="Baby" component={ShopStackNavigator} />
      <Tab.Screen name="ShopStackNavigator" component={ShopStackNavigator} />
      <Tab.Screen name="Support" component={SupportScreen} />
    </Tab.Navigator>
  );
};
