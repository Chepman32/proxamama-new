import { createNavigationContainerRef } from '@react-navigation/native';

export const navigationRef = createNavigationContainerRef();

export const navigationInstance = {
  get navigation() {
    if (navigationRef.isReady()) {
      return navigationRef;
    }
  },
};
