import styled from '@emotion/native';

import { Button } from '~/atoms/common';

export const IntroBottomContainer = styled.View`
  position: absolute;
  bottom: -2px;
  width: 100%;
  height: 80px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-left: 24px;
  padding-right: 24px;
  background-color: ${({ theme }) => theme.palette.white};
`;

export const LoginButton = styled(Button)`
  width: 114px;
`;
