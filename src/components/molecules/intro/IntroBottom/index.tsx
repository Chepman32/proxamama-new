import React from 'react';

import { useNavigation } from '@react-navigation/native';

import { TransparentButton } from '~/atoms/common';

import { IntroBottomContainer, LoginButton } from './styles';

export const IntroBottom = () => {
  const navigation = useNavigation();

  return (
    <IntroBottomContainer>
      <TransparentButton
        title="Sign up"
        onPress={() => navigation.navigate('Signup')}
      />
      <LoginButton
        title="Log in"
        onPress={() => navigation.navigate('Login')}
      />
    </IntroBottomContainer>
  );
};
