import { FC } from 'react';

import styled from '@emotion/native';

import { InfoMessage } from '~/molecules/common';
import { InfoMessageProps } from '~/molecules/common/InfoMessage/types';

export const ShopInfoMessage = styled<
  FC<InfoMessageProps & FirstLastPositionProps>
>(InfoMessage)`
  margin-horizontal: 24px;
  ${({ isFirst }) => isFirst && 'margin-top: 64px;'}
  ${({ isLast }) => !isLast && 'margin-bottom: 16px;'}
`;
