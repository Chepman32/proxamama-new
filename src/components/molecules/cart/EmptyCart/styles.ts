import styled from '@emotion/native';

import { Text } from '~/atoms/common';
import { Cart } from '~/atoms/icons';

export const EmptyCartContainer = styled.View`
  flex-grow: 1;
  align-items: center;
  justify-content: center;
`;

export const EmptyCartTitle = styled(Text)`
  font-size: 20px;
  font-weight: 500;
  text-align: center;
  color: ${({ theme }) => theme.palette.greekAubergine50};
`;

export const EmptyCartDescription = styled(Text)`
  font-size: 12px;
  font-weight: 500;
  text-align: center;
  color: ${({ theme }) => theme.palette.greekAubergine50};
  margin-top: 16px;
`;

export const EmptyCartDescriptionDedicated = styled(EmptyCartDescription)`
  color: ${({ theme }) => theme.palette.greekAubergine};
`;

export const CartIconStyled = styled(Cart)`
  margin-bottom: 16px;
`;
