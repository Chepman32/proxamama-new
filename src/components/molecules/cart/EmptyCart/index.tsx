import React, { FC } from 'react';

import {
  CartIconStyled,
  EmptyCartContainer,
  EmptyCartDescription,
  EmptyCartDescriptionDedicated,
  EmptyCartTitle,
} from './styles';

export const EmptyCart: FC = () => {
  return (
    <EmptyCartContainer>
      <CartIconStyled />
      <EmptyCartTitle>Looks like your{'\n'}cart is empty.</EmptyCartTitle>
      <EmptyCartDescription>
        Check out{' '}
        <EmptyCartDescriptionDedicated>bottles</EmptyCartDescriptionDedicated>,{' '}
        <EmptyCartDescriptionDedicated>bundles</EmptyCartDescriptionDedicated>{' '}
        {'\n'} and{' '}
        <EmptyCartDescriptionDedicated>
          accessories
        </EmptyCartDescriptionDedicated>
      </EmptyCartDescription>
    </EmptyCartContainer>
  );
};
