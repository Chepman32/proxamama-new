import styled from '@emotion/native';

import { Text } from '~/atoms/common';

export const CartItemContainer = styled.View<FirstLastPositionProps>`
  flex-direction: row;
  ${({ isFirst }) => !isFirst && 'margin-top: 30px;'}
`;

export const CartItemImage = styled.Image`
  width: 152px;
  height: 152px;
  border-radius: 8px;
`;

export const CartItemRight = styled.View`
  flex-shrink: 1;
  margin-left: 15px;
`;

export const CartItemTitle = styled(Text)`
  font-size: 12px;
  font-weight: 600;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;

export const CartItemDescription = styled(Text)`
  font-size: 12px;
  color: ${({ theme }) => theme.palette.greekAubergine60};
`;

export const CartItemPrice = styled(Text)`
  margin-top: 12px;
  font-size: 16px;
  font-weight: 500;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;

export const CounterWrapper = styled.View`
  flex: 1;
  justify-content: flex-end;
`;
