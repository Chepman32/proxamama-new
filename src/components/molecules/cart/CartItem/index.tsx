import React, { FC } from 'react';
import { ViewProps } from 'react-native';

import testImage from '~/assets/images/addons/nursingFeeding/3.png';
import { Counter } from '~/molecules/common';

import {
  CartItemContainer,
  CartItemDescription,
  CartItemImage,
  CartItemPrice,
  CartItemRight,
  CartItemTitle,
  CounterWrapper,
} from './styles';

export const CartItem: FC<FirstLastPositionProps & ViewProps> = ({
  isFirst,
  isLast,
  ...props
}) => {
  return (
    <CartItemContainer isFirst={isFirst} isLast={isLast} {...props}>
      <CartItemImage source={testImage} resizeMode="cover" />
      <CartItemRight>
        <CartItemTitle>Newborn Starter Kit</CartItemTitle>
        <CartItemDescription>
          Gray, 10 - 12mm, Skin Tone 3 Tone 3
        </CartItemDescription>
        <CartItemPrice>$120</CartItemPrice>
        <CounterWrapper>
          <Counter value={1} />
        </CounterWrapper>
      </CartItemRight>
    </CartItemContainer>
  );
};
