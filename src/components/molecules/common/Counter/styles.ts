import styled from '@emotion/native';

import { Text } from '~/atoms/common';

export const CounterContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const CounterButton = styled.TouchableOpacity`
  width: 32px;
  height: 32px;
  border-radius: 32px;
  justify-content: center;
  align-items: center;
  background-color: ${({ theme }) => theme.palette.white};
`;

export const CounterValue = styled(Text)`
  font-size: 14px;
  font-weight: 600;
  width: 38px;
  text-align: center;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
