import React, { FC, useCallback, useState } from 'react';
import { ViewProps } from 'react-native';

import { Minus, Plus } from '~/atoms/icons';

import { CounterButton, CounterContainer, CounterValue } from './styles';

interface Props extends ViewProps {
  value?: number;
  handleChange?: (nextCount: number) => void;
}

export const Counter: FC<Props> = ({ value, handleChange, ...props }) => {
  const [count, setCount] = useState(value ? value : 0);

  const onPress = useCallback(
    (type: 'decrement' | 'increment') => () => {
      const nextCount =
        type === 'increment' ? count + 1 : count > 0 ? count - 1 : 0;
      setCount(nextCount);
      if (handleChange) {
        handleChange(nextCount);
      }
    },
    [count, handleChange],
  );

  return (
    <CounterContainer {...props}>
      <CounterButton onPress={onPress('decrement')}>
        <Minus />
      </CounterButton>
      <CounterValue>{count}</CounterValue>
      <CounterButton onPress={onPress('increment')}>
        <Plus />
      </CounterButton>
    </CounterContainer>
  );
};
