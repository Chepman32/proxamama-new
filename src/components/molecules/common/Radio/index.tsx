import React, { FC, memo } from 'react';
import { StyleProp, TextStyle, TouchableOpacityProps } from 'react-native';

import { FieldErrorMessage } from '~/atoms/common';

import { Circle, RememberMeContainer, RememberMeText } from './styles';

interface Props extends TouchableOpacityProps {
  label: string;
  customTextStyle?: StyleProp<TextStyle>;
  checked?: boolean;
  errorMessage?: string;
}

export const Radio: FC<Props> = memo(
  ({ label, customTextStyle, checked, errorMessage, ...otherProps }) => {
    return (
      <RememberMeContainer {...otherProps}>
        <Circle isChecked={!!checked} />
        <RememberMeText style={customTextStyle}>{label}</RememberMeText>
        {errorMessage ? (
          <FieldErrorMessage>{errorMessage}</FieldErrorMessage>
        ) : null}
      </RememberMeContainer>
    );
  },
);
