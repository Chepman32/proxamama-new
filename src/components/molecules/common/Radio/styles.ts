import styled from '@emotion/native';

import { Text } from '~/atoms/common';

export const Circle = styled.View<{ isChecked: boolean }>`
  width: 20px;
  height: 20px;
  margin-right: 15px;
  border-radius: 50px;
  border-width: 2px;
  border-color: ${({ theme }) => theme.palette.greekAubergine};
  ${({ isChecked, theme }) =>
    isChecked && `background-color: ${theme.palette.greekAubergine};`}
`;

export const RememberMeText = styled(Text)`
  color: ${({ theme }) => theme.palette.greekAubergine};
  font-size: 16px;
`;

export const RememberMeContainer = styled.TouchableOpacity`
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;
