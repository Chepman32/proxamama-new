import styled from '@emotion/native';

import { Button, Text } from '~/atoms/common';

export const InfoConfirmContainer = styled.View`
  background-color: ${({ theme }) => theme.palette.placeboOrange};
  padding: 24px;
  border-radius: 16px;
`;

export const InfoConfirmTitle = styled(Text)`
  font-size: 20px;
  font-weight: bold;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;

export const InfoConfirmDescription = styled(Text)`
  margin-top: 8px;
  font-size: 12px;
  font-weight: 500;
  color: ${({ theme }) => theme.palette.greekAubergine50};
`;

export const CloseButton = styled.TouchableOpacity`
  position: absolute;
  top: 24px;
  right: 24px;
  z-index: 1;
`;

export const ConfirmButton = styled(Button)`
  align-self: flex-start;
  padding-horizontal: 20px;
  margin-top: 28px;
`;
