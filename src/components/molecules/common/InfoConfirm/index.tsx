import React, { FC, useState } from 'react';
import { StyleProp, ViewStyle } from 'react-native';

import { Close } from '~/atoms/icons';

import {
  CloseButton,
  ConfirmButton,
  InfoConfirmContainer,
  InfoConfirmDescription,
  InfoConfirmTitle,
} from './styles';

interface Props {
  title: string;
  description: string;
  buttonTitle: string;
  onConfirm?: () => {};
  style?: StyleProp<ViewStyle>;
}

export const InfoConfirm: FC<Props> = ({
  title,
  description,
  buttonTitle,
  onConfirm,
  style,
}) => {
  const [visible, setVisible] = useState(true);
  return visible ? (
    <InfoConfirmContainer style={style}>
      <CloseButton onPress={() => setVisible(false)}>
        <Close />
      </CloseButton>
      <InfoConfirmTitle>{title}</InfoConfirmTitle>
      <InfoConfirmDescription>{description}</InfoConfirmDescription>
      <ConfirmButton title={buttonTitle} onPress={onConfirm} />
    </InfoConfirmContainer>
  ) : null;
};
