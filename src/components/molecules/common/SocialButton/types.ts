import { GestureResponderEvent, StyleProp, ViewStyle } from 'react-native';

export interface SocialButtonProps {
  label: string;
  socialType: 'apple' | 'facebook' | 'google';
  onPress?: (event: GestureResponderEvent) => void;
  style?: StyleProp<ViewStyle>;
}
