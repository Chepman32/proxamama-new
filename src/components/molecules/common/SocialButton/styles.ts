import styled, { css } from '@emotion/native';

import { Text } from '~/atoms/common';

export const SocialButtonContainer = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  height: 64px;
  border-radius: 80px;
  background-color: ${({ theme }) => theme.palette.white};
`;

export const socialIconStyle = css`
  width: 20px;
  height: 20px;
  justify-content: center;
  align-items: center;
  margin-left: 8px;
`;

export const SocialButtonText = styled(Text)`
  font-size: 16px;
  font-weight: 600;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
