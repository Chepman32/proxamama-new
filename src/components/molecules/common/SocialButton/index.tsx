import React, { FC } from 'react';

import { Apple, Facebook, Google } from '~/atoms/icons';

import {
  SocialButtonContainer,
  SocialButtonText,
  socialIconStyle,
} from './styles';
import { SocialButtonProps } from './types';

export const SocialButton: FC<SocialButtonProps> = ({
  style,
  onPress,
  label,
  socialType,
}) => {
  return (
    <SocialButtonContainer onPress={onPress} style={style}>
      <SocialButtonText>{label}</SocialButtonText>
      {socialType === 'google' && <Google style={socialIconStyle} />}
      {socialType === 'facebook' && <Facebook style={socialIconStyle} />}
      {socialType === 'apple' && <Apple style={socialIconStyle} />}
    </SocialButtonContainer>
  );
};
