import React, { FC } from 'react';

import { useTheme } from '@emotion/react';

import { ColorBody } from './styles';
interface IColorPicker {
  color: string;
  handler?: any;
  selected: string;
}

export const ColorPicker: FC<IColorPicker> = ({ color, handler, selected }) => {
  const { palette } = useTheme();
  const bodyStyles = {
    backgroundColor: color,
    borderWidth: color === selected ? 3 : 0,
    borderColor: palette.greekAubergine,
  };
  return (
    <ColorBody
      onPress={() => handler(color)}
      activeOpacity={1}
      style={bodyStyles}
    />
  );
};
