import styled from '@emotion/native';
export const ColorBody = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
  overflow: hidden;
  border-radius: 20px;
`;
