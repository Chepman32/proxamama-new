import { ViewProps } from 'react-native';

export interface InfoMessageProps extends ViewProps {
  title: string;
  description: string;
}
