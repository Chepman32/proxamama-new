import React, { FC } from 'react';

import {
  InfoMessageContainer,
  InfoMessageDescription,
  InfoMessageTitle,
} from './styles';
import { InfoMessageProps } from './types';

export const InfoMessage: FC<InfoMessageProps> = ({
  title,
  description,
  ...otherProps
}) => {
  return (
    <InfoMessageContainer {...otherProps}>
      <InfoMessageTitle>{title}</InfoMessageTitle>
      <InfoMessageDescription>{description}</InfoMessageDescription>
    </InfoMessageContainer>
  );
};
