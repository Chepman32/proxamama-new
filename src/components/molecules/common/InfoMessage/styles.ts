import styled from '@emotion/native';

import { Text } from '~/atoms/common';

export const InfoMessageContainer = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.palette.placeboOrange};
  padding: 16px;
  border-radius: 16px;
`;

export const InfoMessageTitle = styled(Text)`
  font-size: 16px;
  font-weight: bold;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;

export const InfoMessageDescription = styled(Text)`
  margin-top: 4px;
  font-size: 12px;
  font-weight: 500;
  color: ${({ theme }) => theme.palette.greekAubergine50};
`;
