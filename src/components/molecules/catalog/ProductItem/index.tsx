import React, { FC } from 'react';
import {
  ImageSourcePropType,
  ImageStyle,
  TextStyle,
  TouchableOpacityProps,
} from 'react-native';

import {
  ProductItemContainer,
  ProductItemImage,
  ProductItemPrice,
  ProductItemTitle,
} from './styles';

export interface ProductItemProps extends TouchableOpacityProps {
  imageSource: ImageSourcePropType;
  title: string;
  price: string;
  imageStyle?: ImageStyle;
  titleStyle?: TextStyle;
  priceStyle?: TextStyle;
}

export const ProductItem: FC<ProductItemProps> = ({
  imageSource,
  title,
  price,
  imageStyle,
  titleStyle,
  priceStyle,
  ...otherProps
}) => {
  return (
    <ProductItemContainer {...otherProps}>
      <ProductItemImage
        source={imageSource}
        resizeMode="cover"
        style={imageStyle}
      />
      <ProductItemTitle style={titleStyle}>{title}</ProductItemTitle>
      <ProductItemPrice style={priceStyle}>${price}</ProductItemPrice>
    </ProductItemContainer>
  );
};
