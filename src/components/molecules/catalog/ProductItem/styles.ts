import styled from '@emotion/native';

import { Text } from '~/atoms/common';

export const ProductItemContainer = styled.TouchableOpacity``;

export const ProductItemImage = styled.Image`
  max-width: 100%;
  height: 368px;
  border-radius: 16px;
`;

export const ProductItemTitle = styled(Text)`
  font-size: 16px;
  font-weight: bold;
  color: ${({ theme }) => theme.palette.greekAubergine};
  margin-top: 16px;
`;

export const ProductItemPrice = styled(Text)`
  font-size: 16px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
