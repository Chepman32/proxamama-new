import React, { FC } from 'react';
import { ViewStyle } from 'react-native';

import {
  HorizontalCategoriesContainer,
  horizontalCategoriesContainerContentStyle,
  HorizontalCategoriesItem,
  HorizontalCategoriesItemText,
} from './styles';

interface Props {
  style?: ViewStyle;
}

export const HorizontalCategories: FC<Props> = ({ style }) => {
  return (
    <HorizontalCategoriesContainer
      contentContainerStyle={horizontalCategoriesContainerContentStyle}
      horizontal
      showsHorizontalScrollIndicator={false}
      style={style}
    >
      <HorizontalCategoriesItem isSelected>
        <HorizontalCategoriesItemText isSelected>
          View all
        </HorizontalCategoriesItemText>
      </HorizontalCategoriesItem>
      <HorizontalCategoriesItem>
        <HorizontalCategoriesItemText>
          Bottle Add-ons
        </HorizontalCategoriesItemText>
      </HorizontalCategoriesItem>
      <HorizontalCategoriesItem>
        <HorizontalCategoriesItemText>
          Nursing & Feeding
        </HorizontalCategoriesItemText>
      </HorizontalCategoriesItem>
      <HorizontalCategoriesItem>
        <HorizontalCategoriesItemText>Transport</HorizontalCategoriesItemText>
      </HorizontalCategoriesItem>
      <HorizontalCategoriesItem>
        <HorizontalCategoriesItemText>Cleaning</HorizontalCategoriesItemText>
      </HorizontalCategoriesItem>
      <HorizontalCategoriesItem>
        <HorizontalCategoriesItemText>Bundles</HorizontalCategoriesItemText>
      </HorizontalCategoriesItem>
    </HorizontalCategoriesContainer>
  );
};
