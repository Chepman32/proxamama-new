import styled, { css } from '@emotion/native';

import { Text } from '~/atoms/common';

export const HorizontalCategoriesContainer = styled.ScrollView`
  overflow: hidden;
`;

export const horizontalCategoriesContainerContentStyle = css`
  padding-horizontal: 24px;
`;

export const HorizontalCategoriesItem = styled.TouchableOpacity<
  { isSelected?: boolean } & FirstLastPositionProps
>`
  justify-content: center;
  height: 44px;
  padding-horizontal: 20px;
  border-radius: 80px;
  ${({ isSelected, theme }) =>
    isSelected
      ? `background-color: ${theme.palette.greekAubergine};`
      : `border: 2px solid ${theme.palette.greekAubergine};`}
  ${({ isLast }) => !isLast && 'margin-right: 8px'};
`;

export const HorizontalCategoriesItemText = styled(Text)<{
  isSelected?: boolean;
}>`
  font-size: 14px;
  font-weight: 500;
  color: ${({ theme, isSelected }) =>
    isSelected ? theme.palette.white : theme.palette.greekAubergine};
`;
