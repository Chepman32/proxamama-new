/* eslint-disable no-unused-vars */
import * as React from 'react';
import { Text, Image } from 'react-native';
import Carousel from 'react-native-snap-carousel';

import { BottomCarouselItem, BottomCarouselTitle } from './styles';

export const BottomCarousel = () => {
  const [activeIndex, setActiveIndex] = React.useState(0);
  const [carouselItems] = React.useState([
    {
      title: 'Item 1',
      price: '$50',
      image: require('../../assets/images/productsImages/1.png'),
    },
    {
      title: 'Item 2',
      price: '$40',
      image: require('../../assets/images/productsImages/1.png'),
    },
    {
      title: 'Item 3',
      price: 'Text 3',
      image: require('../../assets/images/productsImages/1.png'),
    },
    {
      title: 'Item 4',
      price: '$50',
      image: require('../../assets/images/productsImages/1.png'),
    },
    {
      title: 'Item 5',
      price: '$40',
      image: require('../../assets/images/productsImages/1.png'),
    },
  ]);

  function _renderItem({ item, index }) {
    return (
      <BottomCarouselItem>
        <Image width={250} height={250} source={item.image} />
        <BottomCarouselTitle>{item.title}</BottomCarouselTitle>
        <Text>{item.price}</Text>
      </BottomCarouselItem>
    );
  }

  return (
    <Carousel
      layout={'default'}
      ref={ref => (this.carousel = ref)}
      data={carouselItems}
      sliderWidth={300}
      itemWidth={300}
      renderItem={_renderItem}
      onSnapToItem={index => setActiveIndex(index)}
    />
  );
};
