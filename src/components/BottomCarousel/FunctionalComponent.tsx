/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
import * as React from 'react';
import { Text, View, Image } from 'react-native';
import Carousel from 'react-native-snap-carousel';

import { BottomCarouselItem, BottomCarouselTitle } from './styles';

interface IBottomCarouselItem {
  title: string;
  image: any;
  price: string;
}
interface IBottomCarousel {
  data?: IBottomCarouselItem[];
}
const items = [
  {
    title: 'Item 1',
    price: '$50',
    image: require('../../assets/images/productsImages/1.png'),
  },
  {
    title: 'Item 1',
    price: '$54',
    image: require('../../assets/images/productsImages/1.png'),
  },
  {
    title: 'Item 1',
    price: '$50',
    image: require('../../assets/images/productsImages/1.png'),
  },
  {
    title: 'Item 1',
    price: '$54',
    image: require('../../assets/images/productsImages/1.png'),
  },
];
export const BottomCarousel: React.FC<IBottomCarousel> = ({ data }) => {
  const [activeIndex, setActiveIndex] = React.useState(0);

  const _renderItem = (item: any, index: any) => {
    return (
      <BottomCarouselItem>
        <Image
          width={250}
          height={250}
          source={require('../../assets/images/productsImages/1.png')}
        />
        <BottomCarouselTitle>{item.title}</BottomCarouselTitle>
        <Text>{item.price}</Text>
      </BottomCarouselItem>
    );
  };
  let carousel;
  return (
    <Carousel
      layout={'default'}
      ref={ref => (carousel = ref)}
      data={data ?? items}
      sliderWidth={300}
      itemWidth={300}
      // @ts-ignore
      renderItem={_renderItem}
      onSnapToItem={(index: number) => setActiveIndex(index)}
    />
  );
};
