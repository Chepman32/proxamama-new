import styled from '@emotion/native';

import { deviceHeight } from '~/constants/dimensions';

export const BottomCarouselItem = styled.View`
width: deviceWidth * 0.8,
          border-radius: 5px;
          height: ${deviceHeight * 0.6};
          padding: 10px,
          margin-left: 25px,
          margin-right: 25px,
          justify-content: center
          `;
export const BottomCarouselTitle = styled.Text`
  margin-vertical: 10px;
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  letter-spacing: -0.5px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
