import { FC, memo } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';

import styled, { css } from '@emotion/native';
import { ifProp } from 'styled-tools';

import { SubmitButton } from '~/atoms/auth';
import { Heading, Link, Row, Text, TextInput } from '~/atoms/common';
import { TextInputProps } from '~/atoms/common/TextInput/types';
import { SocialButton } from '~/molecules/common';
import { SocialButtonProps } from '~/molecules/common/SocialButton/types';

export const SafeAreaViewStyled = styled(SafeAreaView)`
  flex: 1;
  background-color: ${({ theme }) => theme.palette.placeboOrange};
`;

export const contentContainerStyle = css`
  padding-horizontal: 24px;
  padding-bottom: 72px;
`;

export const HeadingStyled = styled(Heading)`
  text-align: center;
`;

export const SignupButton = styled(SubmitButton)``;

export const TextInputStyled = memo(styled<
  FC<TextInputProps & FirstLastPositionProps>
>(TextInput)`
  ${({ isFirst }) => !isFirst && 'margin-top: 20px;'}
`);

export const SocialButtonStyled = styled<
  FC<
    SocialButtonProps & {
      isLast?: true;
    }
  >
>(SocialButton)`
  margin-bottom: ${ifProp({ isLast: true }, '0px', '19px')};
`;

export const ForgotPasswordLink = styled(Link)`
  align-items: center;
  margin-top: 33px;
`;

export const SignupNavigationArea = styled(Row)`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 12px;
`;

export const SignupNavigationAreaText = styled(Text)`
  font-size: 14px;
  font-weight: 400;
  color: ${prop => prop.theme.palette.greekAubergine};
  margin-right: 5px;
`;

export const customErrorMessageStyle = css`
  bottom: -16px;
`;

export const radioCustomTextStyle = css`
  font-size: 12px;
`;

export const agreementRadioButton = css`
  margin-top: 39px;
  margin-bottom: 29px;
`;

export const TermsArea = styled.View`
  align-items: center;
  margin-top: 12px;
`;

export const TermsAreaText = styled(Text)<{ isUnderline?: boolean }>`
  font-size: 12px;
  color: ${({ theme }) => theme.palette.greekAubergine70};
  ${({ isUnderline, theme }) =>
    isUnderline &&
    `text-decoration: underline; text-decoration-color: ${theme.palette.greekAubergine70};`}
`;

export const TermsAreaLink = styled.TouchableOpacity`
  padding-horizontal: 5px;
`;
