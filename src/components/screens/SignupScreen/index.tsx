/* eslint-disable react-hooks/rules-of-hooks */
import React, { FC, useCallback } from 'react';
import { Linking, ScrollView } from 'react-native';

import { Formik } from 'formik';
import * as Yup from 'yup';

import { FormContainer } from '~/atoms/auth';
import { TextSeparator } from '~/atoms/common';
import { Radio } from '~/molecules/common';
import { signupSubmit } from '~/services/forms';
import { SignupFormInitialValues } from '~/services/forms/signupSubmit/types';
import { appleAuth, facebookAuth, googleAuth } from '~/services/socialAuth';

import {
  agreementRadioButton,
  contentContainerStyle,
  customErrorMessageStyle,
  HeadingStyled,
  radioCustomTextStyle,
  SafeAreaViewStyled,
  SignupButton,
  SocialButtonStyled,
  TermsArea,
  TermsAreaLink,
  TermsAreaText,
  TextInputStyled,
} from './styles';

const signupFormIntialValues: SignupFormInitialValues = {
  provider_name: 'shopify',
  first_name: '',
  last_name: '',
  email: '',
  password: '',
  confirm_password: '',
  privacy_policy: false,
};

const SignupSchema = Yup.object().shape({
  first_name: Yup.string().required('Required'),
  last_name: Yup.string().required('Required'),
  email: Yup.string().email('Invalid email').required('Required'),
  password: Yup.string()
    .required('Required')
    .min(8, 'Password must contain at least 8 characters'),
  confirm_password: Yup.string()
    .required('Required')
    .when('password', {
      is: (val: string) => val && val.length > 0,
      then: Yup.string().oneOf(
        [Yup.ref('password')],
        'Both password need to be the same',
      ),
    }),
  privacy_policy: Yup.bool().isTrue(
    'You have not accepted the terms of the agreement',
  ),
});

export const SignupScreen: FC = () => {
  return (
    <SafeAreaViewStyled edges={['bottom']}>
      <ScrollView contentContainerStyle={contentContainerStyle}>
        <HeadingStyled>Let’s get you signed up</HeadingStyled>
        <FormContainer>
          <Formik
            initialValues={signupFormIntialValues}
            validationSchema={SignupSchema}
            onSubmit={signupSubmit}
          >
            {({
              handleChange,
              handleSubmit,
              values,
              errors,
              isSubmitting,
              setFieldValue,
            }) => {
              const setPrivacyPolicy = useCallback(() => {
                setFieldValue('privacy_policy', !values.privacy_policy);
              }, [setFieldValue, values.privacy_policy]);

              return (
                <>
                  <SocialButtonStyled
                    label="Continue with"
                    socialType="google"
                    onPress={googleAuth}
                  />
                  <SocialButtonStyled
                    label="Continue with"
                    socialType="facebook"
                    onPress={facebookAuth}
                  />
                  <SocialButtonStyled
                    label="Continue with"
                    socialType="apple"
                    onPress={appleAuth}
                    isLast
                  />
                  <TextSeparator innerContent="or" />
                  <TextInputStyled
                    fieldName="first_name"
                    label={'First Name'}
                    autoCapitalize="none"
                    returnKeyType="next"
                    handleChange={handleChange}
                    value={values.first_name}
                    errorMessage={errors.first_name}
                    customErrorMessageStyle={customErrorMessageStyle}
                    isFirst
                  />
                  <TextInputStyled
                    fieldName="last_name"
                    label={'Last Name'}
                    autoCapitalize="none"
                    returnKeyType="next"
                    handleChange={handleChange}
                    value={values.last_name}
                    errorMessage={errors.last_name}
                    customErrorMessageStyle={customErrorMessageStyle}
                  />
                  <TextInputStyled
                    fieldName="email"
                    label={'Email Address'}
                    autoCapitalize="none"
                    returnKeyType="next"
                    handleChange={handleChange}
                    value={values.email}
                    errorMessage={errors.email}
                    customErrorMessageStyle={customErrorMessageStyle}
                  />
                  <TextInputStyled
                    fieldName="password"
                    label={'Password'}
                    autoCapitalize="none"
                    handleChange={handleChange}
                    value={values.password}
                    errorMessage={errors.password}
                    customErrorMessageStyle={customErrorMessageStyle}
                    returnKeyType="next"
                    isPassword
                  />
                  <TextInputStyled
                    fieldName="confirm_password"
                    label={'Confirm Password'}
                    autoCapitalize="none"
                    handleChange={handleChange}
                    value={values.confirm_password}
                    errorMessage={errors.confirm_password}
                    customErrorMessageStyle={customErrorMessageStyle}
                    returnKeyType="next"
                    isPassword
                  />
                  <Radio
                    style={agreementRadioButton}
                    customTextStyle={radioCustomTextStyle}
                    checked={values.privacy_policy}
                    onPress={setPrivacyPolicy}
                    label="I agree to receive communication to help improve my product experience"
                    errorMessage={errors.privacy_policy}
                  />
                  <SignupButton
                    disabled={isSubmitting}
                    title="Create account"
                    onPress={handleSubmit}
                  />
                  <TermsArea>
                    <TermsAreaText>
                      By creating an account you agree to our
                    </TermsAreaText>
                    <TermsAreaLink
                      onPress={() => Linking.openURL('http://proxamama.com/')}
                    >
                      <TermsAreaText isUnderline>
                        Terms of Service
                      </TermsAreaText>
                    </TermsAreaLink>
                    <TermsAreaText>and</TermsAreaText>
                    <TermsAreaLink
                      onPress={() => Linking.openURL('http://proxamama.com/')}
                    >
                      <TermsAreaText isUnderline>Privacy Policy</TermsAreaText>
                    </TermsAreaLink>
                  </TermsArea>
                </>
              );
            }}
          </Formik>
        </FormContainer>
      </ScrollView>
    </SafeAreaViewStyled>
  );
};
