import { SafeAreaView } from 'react-native-safe-area-context';

import styled from '@emotion/native';

import { Text } from '~/atoms/common';

export const SafeAreaViewStyled = styled(SafeAreaView)`
  flex: 1;
  background-color: ${({ theme }) => theme.palette.white};
`;
export const SupportContainer = styled.View`
  padding-top: 22px;
  padding-horizontal: 24px;
  flex: 1;
  align-items: center;
`;
export const SupportTitle = styled(Text)`
  margin-top: 30px;
  margin-bottom: 32px;
  font-size: 32px;
  line-height: 40px;
  letter-spacing: -1px;
  color: ${({ theme }) => theme.palette.placeboOrange};
`;

export const SupportContact = styled.View`
  margin-bottom: 16px;
  padding: 16px;
  background: ${({ theme }) => theme.palette.placeboOrange};
`;
export const SupportContactTitle = styled(Text)`
  margin-bottom: 6px;
  font-weight: normal;
  font-size: 12px;
  letter-spacing: -0.2px;
  border-radius: 16px;
`;
export const SupportContactText = styled(Text)`
  font-weight: normal;
  font-size: 12px;
  letter-spacing: -0.2px;
  color: rgba(63, 7, 51, 0.5);
`;

export const FAQTitle = styled(Text)`
  margin-top: 22px;
  font-weight: normal;
  font-size: 28px;
  letter-spacing: -1px;
  color: ${({ theme }) => theme.palette.placeboOrange}; ;
`;

export const FAQSubTitle = styled(Text)`
  margin-bottom: 56px;
  font-size: 20px;
  letter-spacing: -0.5px;
  color: ${({ theme }) => theme.palette.placeboOrange}; ;
`;
export const SupportLink = styled(Text)`
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;
  letter-spacing: -0.5px;
  color: ${({ theme }) => theme.palette.placeboOrange}; ;
`;

export const SupportLinkBold = styled(Text)`
  font-weight: 500;
  font-size: 16px;
  line-height: 24px;
  letter-spacing: -0.5px;
  color: ${({ theme }) => theme.palette.placeboOrange}; ;
`;
