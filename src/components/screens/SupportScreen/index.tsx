import React, { FC } from 'react';
import { ScrollView } from 'react-native';

import { Back } from '~/atoms/common';
import { Accordion } from '~/atoms/common/Accordion';
import { RowComponent } from '~/organisms/RowComponent';

import {
  FAQSubTitle,
  FAQTitle,
  SafeAreaViewStyled,
  SupportContact,
  SupportContactText,
  SupportContactTitle,
  SupportContainer,
  SupportLink,
  SupportLinkBold,
  SupportTitle,
} from './styles';

export const SupportScreen: FC = () => {
  const placeholder =
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';
  return (
    <SafeAreaViewStyled>
      <ScrollView>
        <SupportContainer>
          <Back />
          <SupportTitle>Support</SupportTitle>
          <SupportContact>
            <SupportContactTitle>Contact Us</SupportContactTitle>
            <SupportContactText>
              Dignissim scelerisque eget nibh gravida amet, diam blandit rhoncus
              facilisis. Enim.
            </SupportContactText>
          </SupportContact>
          <SupportContact>
            <SupportContactTitle>Chat with Emulait</SupportContactTitle>
            <SupportContactText>
              Dignissim scelerisque eget nibh gravida amet, diam blandit rhoncus
              facilisis. Enim.
            </SupportContactText>
          </SupportContact>
          <FAQTitle>Frequently Asked Questions</FAQTitle>
          <FAQSubTitle>Bottles</FAQSubTitle>
          <Accordion
            title={'Is emulait baby bottle safe in microwave?'}
            text={placeholder}
          />
          <Accordion
            title={'Are there different nipple flow rates?'}
            text={placeholder}
          />
          <Accordion
            title={'Are your bottles anti-colic?'}
            text={placeholder}
          />
          <Accordion
            title={'Are emulait bottles exempt from BPA and phthalates?'}
            text={placeholder}
          />
          <Accordion
            title={'What do I do if my bottle leaks?'}
            text={placeholder}
          />
          <FAQSubTitle>Cleaning and Maintenance</FAQSubTitle>
          <Accordion
            title={'Are there different nipple flow rates?'}
            text={placeholder}
          />
          <Accordion
            title={'Are your bottles anti-colic?'}
            text={placeholder}
          />
          <Accordion
            title={'Are emulait bottles exempt from BPA and phthalates?'}
            text={placeholder}
          />
          <Accordion
            title={'What do I do if my bottle leaks?'}
            text={placeholder}
          />
          <FAQSubTitle>Orders</FAQSubTitle>
          <Accordion
            title={'Are there different nipple flow rates?'}
            text={placeholder}
          />
          <Accordion
            title={'Are your bottles anti-colic?'}
            text={placeholder}
          />
          <Accordion
            title={'Are emulait bottles exempt from BPA and phthalates?'}
            text={placeholder}
          />
          <Accordion
            title={'What do I do if my bottle leaks?'}
            text={placeholder}
          />
          <SupportTitle>Any more questions?</SupportTitle>
          <RowComponent>
            <SupportLink>Visit us at &nbsp;</SupportLink>
            <SupportLinkBold>emulait.com/support</SupportLinkBold>
          </RowComponent>
        </SupportContainer>
      </ScrollView>
    </SafeAreaViewStyled>
  );
};
