import React, { FC, useState } from 'react';
import { ScrollView, TouchableOpacity, View } from 'react-native';
import Svg, { Circle, Path } from 'react-native-svg';

import { useTheme } from '@emotion/react';
import { BottomCarousel } from 'components/BottomCarousel';
import { Carousel } from 'components/Carousel';
import { Dropdown } from 'components/Dropdown';

import { FullSystemItem } from '~/atoms/common/FullSystemItem';
import { ColorPicker } from '~/molecules/common/ColorPicker';

import {
  FullSystemAddToCartBtn,
  FullSystemAddToCartText,
  FullSystemBottomTitle,
  FullSystemCarouselContainer,
  FullSystemContainer,
  FullSystemDesc,
  FullSystemPrice,
  FullSystemQuantity,
  FullSystemQuantityRow,
  FullSystemSizeTitle,
  FullSystemTitle,
  FullSystemWarning,
  FullSystemWarningBold,
  FullSystemWarningContainer,
  SafeAreaViewStyled,
  SizeTitleContainer,
} from './styles';

export const FullSystemScreen: FC = () => {
  const [selectedSize, setSelectedSize] = useState('');
  const [color, setColor] = useState('');
  const [count, setCount] = useState(0);
  const { palette } = useTheme();
  const colorsStyles = {
    marginTop: 19,
    marginBottom: 27,
    paddingRight: 24,
    paddingLeft: 24,
    flexDirection: 'row',
    justifyContent: 'space-between',
  };
  const quantityHandlerStyles = {
    width: 40,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: palette.placeboOrange,
    overflow: 'hidden',
    borderRadius: 20,
  };
  return (
    <SafeAreaViewStyled>
      <ScrollView>
        <View>
          <FullSystemCarouselContainer>
            <Carousel />
          </FullSystemCarouselContainer>
          <FullSystemContainer>
            <FullSystemTitle>Starter kit feeding system</FullSystemTitle>
            <FullSystemPrice>$100</FullSystemPrice>
            <FullSystemDesc>
              Your starter kit feeding system includes two bottles and two
              nipples.
            </FullSystemDesc>
          </FullSystemContainer>
          <FullSystemItem
            title="Bottle Type A"
            desc="Sagittis porta ac est pharetra, sem nec, quam venenatis..."
          />
          <FullSystemItem
            title="Bottle Type B"
            desc="Urna at gravida dui nec tempor lectus urna, consectetur..."
          />
          <FullSystemItem
            title="Nipple"
            desc={'Size: 16 – 20 mm \n Color: Warm Beige'}
            isBordered={false}
          />
          <FullSystemWarningContainer>
            <FullSystemWarning>
              Need help picking a size? &nbsp;
            </FullSystemWarning>
            <FullSystemWarningBold>Customize your system</FullSystemWarningBold>
          </FullSystemWarningContainer>
          <FullSystemSizeTitle>Nipple color</FullSystemSizeTitle>
          <View
            // @ts-ignore
            style={colorsStyles}
          >
            <ColorPicker
              color={palette.candyMix}
              handler={setColor}
              selected={color}
            />
            <ColorPicker
              color={palette.prosciuto}
              handler={setColor}
              selected={color}
            />
            <ColorPicker
              color={palette.rustyGate}
              handler={setColor}
              selected={color}
            />
            <ColorPicker
              color={palette.burningSand}
              handler={setColor}
              selected={color}
            />
            <ColorPicker
              color={palette.mochaGlow}
              handler={setColor}
              selected={color}
            />
            <ColorPicker
              color={palette.tetsuGuroBlack}
              handler={setColor}
              selected={color}
            />
          </View>
          <FullSystemSizeTitle>Quantity</FullSystemSizeTitle>
          <FullSystemQuantityRow>
            <TouchableOpacity
              onPress={() => count > 0 && setCount(count - 1)}
              // @ts-ignore
              style={quantityHandlerStyles}
            >
              <FullSystemQuantity>-</FullSystemQuantity>
            </TouchableOpacity>
            <FullSystemQuantity>{count} </FullSystemQuantity>
            <TouchableOpacity
              onPress={() => setCount(count + 1)}
              // @ts-ignore
              style={quantityHandlerStyles}
            >
              <FullSystemQuantity>+</FullSystemQuantity>
            </TouchableOpacity>
          </FullSystemQuantityRow>
          <SizeTitleContainer>
            <FullSystemSizeTitle>Nipple size</FullSystemSizeTitle>
            <TouchableOpacity>
              <Svg width={16} height={16} fill="none">
                <Path
                  d="M7.14 9.791h1.237v-.077c.017-.937.303-1.357 1.038-1.784.775-.44 1.25-1.06 1.25-1.976 0-1.324-1.064-2.22-2.651-2.22-1.458 0-2.626.803-2.682 2.27h1.315c.052-.864.705-1.243 1.367-1.243.735 0 1.332.46 1.332 1.181 0 .607-.402 1.035-.917 1.336-.804.464-1.28.925-1.289 2.436v.077Zm.653 2.477c.472 0 .865-.363.865-.815 0-.444-.393-.81-.865-.81-.476 0-.865.366-.865.81 0 .452.39.815.865.815Z"
                  fill="#3F0733"
                />
                <Circle cx={8} cy={8} r={7.5} stroke="#3F0733" />
              </Svg>
            </TouchableOpacity>
          </SizeTitleContainer>

          <Dropdown
            data={[
              '1 – 15 mm',
              '16 – 20 mm (most common)',
              '21 – 25 mm',
              '31+ mm',
            ]}
            handler={setSelectedSize}
            selected={selectedSize}
          />
          <FullSystemContainer>
            <FullSystemBottomTitle>You may also like</FullSystemBottomTitle>
            <BottomCarousel />
          </FullSystemContainer>
          <FullSystemAddToCartBtn>
            <FullSystemAddToCartText>Add to Cart</FullSystemAddToCartText>
          </FullSystemAddToCartBtn>
        </View>
      </ScrollView>
    </SafeAreaViewStyled>
  );
};
