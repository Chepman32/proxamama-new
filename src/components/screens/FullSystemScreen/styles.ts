import { SafeAreaView } from 'react-native-safe-area-context';

import styled from '@emotion/native';

import { Text } from '~/atoms/common';

export const SafeAreaViewStyled = styled(SafeAreaView)`
  flex: 1;
  background-color: ${({ theme }) => theme.palette.white};
`;
export const FullSystemContainer = styled.View`
  padding-bottom: 41px;
  padding-horizontal: 24px;
  flex: 1;
  align-items: center;
`;
export const FullSystemCarouselContainer = styled.View`
  height: 375px;
`;

export const FullSystemTitle = styled(Text)`
  margin-top: 24px;
  margin-bottom: 10px;
  font-weight: normal;
  font-size: 28px;
  letter-spacing: -0.2px;
  border-radius: 16px;
`;

export const FullSystemDesc = styled(Text)`
  margin-top: 10px;
  margin-bottom: 24px;
  font-size: 14px;
  line-height: 20px;
  letter-spacing: -0.3px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;

export const FullSystemPrice = styled.Text`
  margin-bottom: 10px;
  font-weight: normal;
  font-size: 20px;
  letter-spacing: 0.5px;
  border-radius: 16px;
`;
export const FullSystemWarningContainer = styled.View`
  margin-bottom: 35px;
  padding-vertical: 20px;
  padding-horizontal: 35px;
  flex-direction: row;
  overflow: hidden;
  background: ${({ theme }) => theme.palette.springSong};
  border-radius: 16px;
`;

export const FullSystemWarning = styled(Text)`
  font-size: 14px;
  line-height: 20px;
  display: flex;
  align-items: center;
  letter-spacing: -0.3px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;

export const FullSystemWarningBold = styled(Text)`
  font-size: 14px;
  line-height: 20px;
  font-weight: 600;
  align-items: center;
  letter-spacing: -0.3px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
export const SizeTitleContainer = styled.View`
  margin-top: 16px;
  margin-bottom: 24px;
  flex-direction: row;
`;
export const FullSystemSizeTitle = styled.Text`
  margin-right: 4px;
  padding-horizontal: 24px;
  font-size: 12px;
  line-height: 16px;
  letter-spacing: -0.2px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;

export const FullSystemQuantityRow = styled.View`
  margin-top: 16px;
  margin-bottom: 56px;
  margin-left: 24px;
  flex-direction: row;
`;
export const FullSystemQuantityHandler = styled.TouchableOpacity`
  padding: 13px;
  background: ${({ theme }) => theme.palette.placeboOrange};
  overflow: hidden;
  border-radius: 5px;
`;
export const FullSystemQuantity = styled.Text`
  margin-horizontal: 12px;
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  text-align: center;
  letter-spacing: -0.5px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
export const FullSystemBottomCarousel = styled.View`
  margin-bottom: 32px;
  padding-top: 40px;
  padding-left: 24px;
  border-top-width: 1px;
  border-top-color: ${({ theme }) => theme.palette.greekAubergine};
`;
export const FullSystemBottomTitle = styled.Text`
  margin-top: 24px;
  margin-bottom: 10px;
  font-style: normal;
  font-weight: normal;
  font-size: 20px;
  line-height: 25px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
export const FullSystemDropdown = styled.TouchableOpacity`
  margin-top: 16px;
  margin-bottom: 24px;
  padding-vertical: 18px;
  padding-right: 30px;
  padding-left: 24px;
  flex-direction: row;
  overflow: hidden;
  background: ${({ theme }) => theme.palette.placeboOrange};
  border-radius: 80px;
  font-size: 44px;
  line-height: 20px;
  letter-spacing: -0.3px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
export const FullSystemDropdownText = styled.Text`
  padding-right: 14px;
  font-size: 14px;
  line-height: 20px;
  letter-spacing: -0.3px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
export const FullSystemAddToCartBtn = styled.TouchableOpacity`
  width: 90%;
  margin-top: 56px;
  margin-left: 5%;
  padding-vertical: 22px;
  justify-content: center;
  align-itemss: center;
  background: ${({ theme }) => theme.palette.greekAubergine};
  overflow: hidden;
  border-radius: 80px;
`;
export const FullSystemAddToCartText = styled.Text`
  font-size: 14px;
  line-height: 20px;
  display: flex;
  align-items: center;
  text-align: center;
  letter-spacing: -0.3px;
  color: ${({ theme }) => theme.palette.white};
`;

export const FullSystemColors = styled.View`
margin-top: 19px;
margin-bottom: 27px;
flex-dirextion: row';
justify-content: space-between;
`;
