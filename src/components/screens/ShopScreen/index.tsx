import React, { FC } from 'react';
import { StatusBar } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { ShopInfoMessage } from '~/molecules/shop';
import { ShopHeader } from '~/organisms/shop';

import shopCategory1 from './shopCategory1.png';
import shopCategory2 from './shopCategory2.png';
import shopCategory3 from './shopCategory3.png';
import {
  InfoConfirmStyled,
  ShopCategoryStyled,
  ShopContainer,
  ShopContent,
} from './styles';

export const ShopScreen: FC = () => {
  const navigation = useNavigation();
  return (
    <ShopContainer>
      <StatusBar barStyle={'dark-content'} />
      <ShopHeader />
      <ShopContent>
        <InfoConfirmStyled
          title="Activity"
          description="Track your baby’s activities with your caregiving circle."
          buttonTitle="Start tracking"
        />
        <ShopCategoryStyled
          isFirst
          title="Full System"
          description="To help provide you with all of the bottles, nipples, and accessories you need for feeding different ages, we have pre-assembled products for you!"
          buttonTitle="Customize your system"
          onPress={() => navigation.navigate('FullSystem')}
          imageSource={shopCategory1}
        />
        <ShopCategoryStyled
          title="Bottles"
          description="The bottle is incredibly important in the feeding process. This is why we have two dynamic bottle designs complete with a nipple tailored specifically to you."
          buttonTitle="Shop our bottles"
          onPress={() => navigation.navigate('Bottles')}
          imageSource={shopCategory2}
        />
        <ShopCategoryStyled
          isLast
          title="Add-on Products"
          description="We have a variety of accessories from extra nipples to detachable bottle handles for you to choose from."
          buttonTitle="Shop all"
          onPress={() => navigation.navigate('Addons')}
          imageSource={shopCategory3}
        />
        <ShopInfoMessage
          isFirst
          title="My Orders"
          description="Dignissim scelerisque eget nibh gravida amet, diam blandit rhoncus facilisis. Enim."
        />
        <ShopInfoMessage
          title="My Subscriptions"
          description="Turpis scelerisque at egestas praesent ut elementum congue risus. Ornare."
          isLast
        />
      </ShopContent>
    </ShopContainer>
  );
};
