import { FC } from 'react';

import styled from '@emotion/native';

import { InfoConfirm } from '~/molecules/common';
import { ShopCategory } from '~/organisms/shop';
import { ShopCategoryProps } from '~/organisms/shop/ShopCategory/types';

export const ShopContainer = styled.ScrollView`
  background-color: ${({ theme }) => theme.palette.placeboOrange};
`;
export const ShopContent = styled.View`
  background-color: ${({ theme }) => theme.palette.white};
  padding-top: 32px;
  padding-bottom: 56px;
`;

export const InfoConfirmStyled = styled(InfoConfirm)`
  margin-horizontal: 24px;
  margin-bottom: 40px;
`;

export const ShopCategoryStyled = styled<
  FC<ShopCategoryProps & FirstLastPositionProps>
>(ShopCategory)`
  ${({ isLast, theme }) =>
    !isLast &&
    `
      border-bottom-width: 1px;
      border-color: ${theme.palette.greekAubergine15};
      padding-bottom: 56px;
    `}
  ${({ isFirst }) => !isFirst && 'padding-top: 32px;'}
`;
