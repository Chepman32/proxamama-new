import React, { FC } from 'react';
import { StatusBar } from 'react-native';

import introBackground from '~/assets/images/intro/intro_background.png';
import { IntroBottom } from '~/molecules/intro';

import {
  IntroBackground,
  IntroContainer,
  IntroLogoStyled,
  SafeAreaViewStyled,
} from './styles';

export const IntroScreen: FC = () => {
  return (
    <SafeAreaViewStyled edges={['bottom']}>
      <StatusBar barStyle={'dark-content'} />
      <IntroContainer>
        <IntroLogoStyled />
        <IntroBackground resizeMode="cover" source={introBackground} />
        <IntroBottom />
      </IntroContainer>
    </SafeAreaViewStyled>
  );
};
