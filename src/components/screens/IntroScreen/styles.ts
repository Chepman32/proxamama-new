import { SafeAreaView } from 'react-native-safe-area-context';

import styled from '@emotion/native';

import { IntroLogo } from '~/atoms/intro';

export const SafeAreaViewStyled = styled(SafeAreaView)`
  flex: 1;
  background-color: ${({ theme }) => theme.palette.white};
`;

export const IntroContainer = styled.View`
  align-items: center;
  width: 100%;
  height: 100%;
  background-color: ${({ theme }) => theme.palette.white};
`;

export const IntroLogoStyled = styled(IntroLogo)`
  position: absolute;
  top: 68px;
  width: 327px;
  height: 90px;
  z-index: 1;
`;

export const IntroBackground = styled.Image`
  position: absolute;
  top: 0;
  width: 100%;
`;
