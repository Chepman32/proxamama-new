import React, { FC } from 'react';
import { ScrollView } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { Link } from '~/atoms/common';
import { loginSubmit } from '~/services/forms';
import { LoginFormInitialValues } from '~/services/forms/loginSubmit/types';
import { appleAuth, facebookAuth, googleAuth } from '~/services/socialAuth';

import {
  contentContainerStyle,
  ForgotPasswordLink,
  FormContainer,
  Heading,
  RememberMeRadioButton,
  SafeAreaViewStyled,
  SigninButton,
  SignupNavigationArea,
  SignupNavigationAreaText,
  SocialButtonStyled,
  TextInputStyled,
} from './styles';

const loginFormIntialValues: LoginFormInitialValues = {
  email: '',
  password: '',
};

const LoginSchema = Yup.object().shape({
  email: Yup.string().email('Invalid email').required('Required'),
  password: Yup.string().required('Required'),
});

export const LoginScreen: FC = () => {
  const navigation = useNavigation();
  return (
    <SafeAreaViewStyled edges={['bottom']}>
      <ScrollView contentContainerStyle={contentContainerStyle}>
        <Heading>Welcome back</Heading>
        <FormContainer>
          <Formik
            initialValues={loginFormIntialValues}
            validationSchema={LoginSchema}
            onSubmit={loginSubmit}
          >
            {({ handleChange, handleSubmit, values, errors, isSubmitting }) => (
              <>
                <TextInputStyled
                  fieldName="email"
                  label={'Email Address'}
                  autoCapitalize="none"
                  returnKeyType="next"
                  handleChange={handleChange}
                  value={values.email}
                  errorMessage={errors.email}
                  isFirst
                />
                <TextInputStyled
                  fieldName="password"
                  label={'Password'}
                  autoCapitalize="none"
                  handleChange={handleChange}
                  value={values.password}
                  errorMessage={errors.password}
                  returnKeyType="next"
                  isPassword
                />
                <SigninButton
                  title="Sign In"
                  onPress={handleSubmit}
                  disabled={isSubmitting}
                />
                <RememberMeRadioButton label="Remember me" />
                <SocialButtonStyled
                  label="Sign In with "
                  socialType="google"
                  onPress={googleAuth}
                />
                <SocialButtonStyled
                  label="Sign In with "
                  socialType="facebook"
                  onPress={facebookAuth}
                />
                <SocialButtonStyled
                  label="Sign In with "
                  socialType="apple"
                  onPress={appleAuth}
                  isLast
                />
                <ForgotPasswordLink title="I forgot my password" />
                <SignupNavigationArea>
                  <SignupNavigationAreaText>
                    New to Emulait?
                  </SignupNavigationAreaText>
                  <Link
                    title="Sign up now"
                    onPress={() => navigation.navigate('Signup')}
                  />
                </SignupNavigationArea>
              </>
            )}
          </Formik>
        </FormContainer>
      </ScrollView>
    </SafeAreaViewStyled>
  );
};
