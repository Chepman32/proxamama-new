import { FC, memo } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';

import styled, { css } from '@emotion/native';
import { ifProp } from 'styled-tools';

import { SubmitButton } from '~/atoms/auth';
import { Link, Row, Text, TextInput } from '~/atoms/common';
import { TextInputProps } from '~/atoms/common/TextInput/types';
import { Radio, SocialButton } from '~/molecules/common';
import { SocialButtonProps } from '~/molecules/common/SocialButton/types';

export const SafeAreaViewStyled = styled(SafeAreaView)`
  flex: 1;
  background-color: ${({ theme }) => theme.palette.placeboOrange};
`;

export const contentContainerStyle = css`
  padding-horizontal: 24px;
  padding-bottom: 72px;
`;

export const Heading = styled(Text)`
  font-size: 32px;
  font-weight: 500;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;

export const FormContainer = styled.View`
  margin-top: 44px;
`;

export const SigninButton = styled(SubmitButton)`
  margin-top: 32px;
`;

export const TextInputStyled = memo(styled<
  FC<TextInputProps & FirstLastPositionProps>
>(TextInput)`
  ${({ isFirst }) => !isFirst && 'margin-top: 32px;'}
`);

export const SocialButtonStyled = styled<
  FC<
    SocialButtonProps & {
      isLast?: true;
    }
  >
>(SocialButton)`
  margin-bottom: ${ifProp({ isLast: true }, '0px', '19px')};
`;

export const ForgotPasswordLink = styled(Link)`
  align-items: center;
  margin-top: 33px;
`;

export const SignupNavigationArea = styled(Row)`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 12px;
`;

export const SignupNavigationAreaText = styled(Text)`
  font-size: 14px;
  font-weight: 400;
  color: ${({ theme }) => theme.palette.greekAubergine};
  margin-right: 5px;
`;

export const RememberMeRadioButton = styled(Radio)`
  margin-vertical: 32px;
`;
