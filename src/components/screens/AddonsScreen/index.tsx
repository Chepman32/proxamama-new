import React, { FC } from 'react';

import { AddonsCategories } from '~/organisms/addons';

import {
  AddonsContainer,
  AddonsHeading,
  AddonsHorizontalCategories,
} from './styles';

export const AddonsScreen: FC = () => {
  return (
    <AddonsContainer>
      <AddonsHeading>Add-Ons</AddonsHeading>
      <AddonsHorizontalCategories />
      <AddonsCategories />
    </AddonsContainer>
  );
};
