import styled, { css } from '@emotion/native';

import { Text } from '~/atoms/common';
import { theme as themeFromConfig } from '~/config';
import { HorizontalCategories } from '~/molecules/catalog';

export const AddonsContainer = styled.ScrollView`
  flex: 1;
  background-color: ${({ theme }) => theme.palette.white};
`;

export const addonsContainerContentStyle = css`
  background-color: ${themeFromConfig.palette.white};
  padding-bottom: 60px;
`;

export const AddonsHeading = styled(Text)`
  font-size: 32px;
  color: ${({ theme }) => theme.palette.greekAubergine};
  margin-bottom: 16px;
  margin-horizontal: 24px;
`;

export const AddonsHorizontalCategories = styled(HorizontalCategories)`
  margin-bottom: 40px;
`;
