import React, { FC, useState } from 'react';
import { ScrollView, TouchableOpacity, View } from 'react-native';

import { useTheme } from '@emotion/react';
import { BottomCarousel } from 'components/BottomCarousel/';
import { Carousel } from 'components/Carousel';
import { Dropdown } from 'components/Dropdown';

import { Accordion } from '~/atoms/common/Accordion';
import { deviceWidth } from '~/constants/dimensions';
import { ColorPicker } from '~/molecules/common/ColorPicker';
import { FeedbackItem } from '~/organisms/FeedbackItem';

import {
  BottleDetailsDesc,
  BottleDetailsBottomCarousel,
  BottleDetailsBottomTitle,
  BottleDetailsCarouselContainer,
  BottleDetailsPrice,
  BottleDetailsQuantity,
  BottleDetailsQuantityRow,
  BottleDetailsSizeTitle,
  BottleDetailsTitle,
  BottleDetailsWarning,
  BottleDetailsWarningBold,
  BottleDetailsWarningContainer,
  SafeAreaViewStyled,
  BottleDetailsListTitle,
  BottleDetailsContainer,
  BottleDetailsAddToCartBtn,
  BottleDetailsAddToCartText,
  BottleDetailsFeedback,
  BottleDetailsFAQTitle,
} from './styles';

export const BottleDetailsScreen: FC = () => {
  const [selectedSize, setSelectedSize] = useState('');
  const [color, setColor] = useState('');
  const [count, setCount] = useState(0);
  const { palette } = useTheme();
  const placeholder =
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';
  const quantityHandlerStyles = {
    width: 40,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: palette.placeboOrange,
    overflow: 'hidden',
    borderRadius: 20,
  };
  const colorsContainerStyles = {
    width: deviceWidth * 0.9,
    marginTop: 19,
    marginBottom: 24,
    flexDirection: 'row',
    justifyContent: 'space-between',
  };
  return (
    <SafeAreaViewStyled>
      <ScrollView>
        <BottleDetailsContainer>
          <BottleDetailsCarouselContainer>
            <Carousel />
          </BottleDetailsCarouselContainer>
          <BottleDetailsTitle>Bottle Type A + Nipple</BottleDetailsTitle>
          <BottleDetailsPrice>$65</BottleDetailsPrice>
          <Dropdown
            data={[
              '1 – 15 mm',
              '16 – 20 mm (most common)',
              '21 – 25 mm',
              '31+ mm',
            ]}
            handler={setSelectedSize}
            selected={selectedSize}
          />
          <BottleDetailsWarningContainer>
            <BottleDetailsWarning>
              Need help picking a size? &nbsp;
            </BottleDetailsWarning>
            <BottleDetailsWarningBold>
              Customize your system
            </BottleDetailsWarningBold>
          </BottleDetailsWarningContainer>
          <BottleDetailsSizeTitle>Nipple color</BottleDetailsSizeTitle>
          <View
            // @ts-ignore
            style={colorsContainerStyles}
          >
            <ColorPicker
              color={palette.candyMix}
              handler={setColor}
              selected={color}
            />
            <ColorPicker
              color={palette.prosciuto}
              handler={setColor}
              selected={color}
            />
            <ColorPicker
              color={palette.rustyGate}
              handler={setColor}
              selected={color}
            />
            <ColorPicker
              color={palette.burningSand}
              handler={setColor}
              selected={color}
            />
            <ColorPicker
              color={palette.mochaGlow}
              handler={setColor}
              selected={color}
            />
            <ColorPicker
              color={palette.tetsuGuroBlack}
              handler={setColor}
              selected={color}
            />
          </View>
          <BottleDetailsSizeTitle>Quantity</BottleDetailsSizeTitle>
          <BottleDetailsQuantityRow>
            <TouchableOpacity
              onPress={() => count > 0 && setCount(count - 1)}
              // @ts-ignore
              style={quantityHandlerStyles}
            >
              <BottleDetailsQuantity>-</BottleDetailsQuantity>
            </TouchableOpacity>
            <BottleDetailsQuantity>{count} </BottleDetailsQuantity>
            <TouchableOpacity
              onPress={() => setCount(count + 1)}
              // @ts-ignore
              style={quantityHandlerStyles}
            >
              <BottleDetailsQuantity>+</BottleDetailsQuantity>
            </TouchableOpacity>
          </BottleDetailsQuantityRow>
          <BottleDetailsDesc>
            Designed with baby in mind, emulait eliminates bottle refusal by
            ensuring baby can latch and control the flow of milk. Our bottles
            are free of BPA, BPS and all chemicals found in traditional baby
            bottles that are known to activate estrogen.
          </BottleDetailsDesc>
          <View>
            <BottleDetailsListTitle>What is included:</BottleDetailsListTitle>
          </View>
          <View>
            <BottleDetailsDesc>&bull; 4oz container</BottleDetailsDesc>
          </View>
          <View>
            <BottleDetailsDesc>&bull; Non-skid base</BottleDetailsDesc>
          </View>
          <View>
            <BottleDetailsDesc>&bull; Travel cap</BottleDetailsDesc>
          </View>
          <View>
            <BottleDetailsDesc>&bull; Care manual</BottleDetailsDesc>
          </View>
        </BottleDetailsContainer>
        <BottleDetailsFeedback>
          <FeedbackItem
            name="Jane D."
            date="8 days ago"
            rating={4}
            text="Both my babies love this bottle. My second baby I think I bought 7-8 different bottles and this one was the one she finally took!"
          />
        </BottleDetailsFeedback>
        <BottleDetailsContainer>
          <BottleDetailsFAQTitle>
            Frequently Asked Questions
          </BottleDetailsFAQTitle>
          <Accordion
            title={'Is emulait baby bottle safe in microwave?'}
            text={placeholder}
          />
          <Accordion
            title={'Are there different nipple flow rates?'}
            text={placeholder}
          />
          <Accordion
            title={'Are your bottles anti-colic?'}
            text={placeholder}
          />
          <Accordion
            title={'Are emulait bottles exempt from BPA and phthalates?'}
            text={placeholder}
          />
          <Accordion
            title={'What do I do if my bottle leaks?'}
            text={placeholder}
          />
          <Accordion
            title={'Are there different nipple flow rates?'}
            text={placeholder}
          />
          <Accordion
            title={'Are your bottles anti-colic?'}
            text={placeholder}
          />
          <Accordion
            title={'Are emulait bottles exempt from BPA and phthalates?'}
            text={placeholder}
          />
          <Accordion
            title={'What do I do if my bottle leaks?'}
            text={placeholder}
          />
        </BottleDetailsContainer>
        <BottleDetailsBottomCarousel>
          <BottleDetailsBottomTitle>You may also like</BottleDetailsBottomTitle>
          <BottomCarousel />
        </BottleDetailsBottomCarousel>
        <BottleDetailsAddToCartBtn>
          <BottleDetailsAddToCartText>Add to Cart</BottleDetailsAddToCartText>
        </BottleDetailsAddToCartBtn>
      </ScrollView>
    </SafeAreaViewStyled>
  );
};
