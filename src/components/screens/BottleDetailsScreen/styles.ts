import { SafeAreaView } from 'react-native-safe-area-context';

import styled from '@emotion/native';

export const SafeAreaViewStyled = styled(SafeAreaView)`
  flex: 1;
  background-color: ${({ theme }) => theme.palette.white};
`;
export const BottleDetailsContainer = styled.View`
padding-bottom: 41px;
padding-horizontal: 24px;
flex: 1;a
border-radius: 80px;
font-size: 44px;
line-height: 20px;
letter-spacing: -0.3px;
  `;
export const BottleDetailsCarouselContainer = styled.View`
  height: 375px;
`;

export const BottleDetailsTitle = styled.Text`
  margin-top: 24px;
  margin-bottom: 10px;
  font-weight: normal;
  font-size: 28px;
  letter-spacing: -0.2px;
  border-radius: 16px;
`;

export const BottleDetailsDesc = styled.Text`
  width: 327px;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  letter-spacing: -0.3px;
  color: ${({ theme }) => theme.palette.greekAubergine};
  color: ${({ theme }) => theme.palette.greekAubergine};
`;

export const BottleDetailsPrice = styled.Text`
  margin-bottom: 10px;
  font-weight: normal;
  font-size: 20px;
  letter-spacing: 0.5px;
  border-radius: 16px;
`;
export const BottleDetailsWarningContainer = styled.View`
  margin-top: 12px;
  margin-bottom: 35px;
  padding: 20px;
  flex-direction: row;
  overflow: hidden;
  background: ${({ theme }) => theme.palette.springSong};
  border-radius: 16px;
`;

export const BottleDetailsWarning = styled.Text`
  font-size: 14px;
  line-height: 20px;
  display: flex;
  align-items: center;
  letter-spacing: -0.3px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;

export const BottleDetailsWarningBold = styled.Text`
  font-size: 14px;
  line-height: 20px;
  font-weight: 600;
  align-items: center;
  letter-spacing: -0.3px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
export const BottleDetailsSizeTitle = styled.Text`
  margin-right: 4px;
  padding-horizontal: 24px;
  font-size: 12px;
  line-height: 16px;
  letter-spacing: -0.2px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
export const BottleDetailsColors = styled.View`
margin-top: 19px;
margin-bottom: 27px;
padding-right: 47px;
padding-left: 28px;
flex-dirextion: row';
justify-content: space-between;
`;
export const BottleDetailsQuantityRow = styled.View`
  margin-top: 16px;
  margin-bottom: 56px;
  flex-direction: row;
  align-items: center;
`;
export const BottleDetailsQuantity = styled.Text`
  margin-horizontal: 12px;
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  text-align: center;
  letter-spacing: -0.5px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
export const BottleDetailsQuantityHandler = styled.TouchableOpacity`
padding: 13,
                backgroundColor: ${({ theme }) => theme.palette.greekAubergine};
                overflow: hidden;
                border-radius: 2px;
`;
export const BottleDetailsBottomCarousel = styled.View`
  margin-bottom: 32px;
  padding-top: 40px;
  padding-left: 24px;
  border-top-width: 1px;
  border-top-color: ${({ theme }) => theme.palette.greekAubergine};
`;
export const BottleDetailsBottomTitle = styled.Text`
  margin-top: 24px;
  margin-bottom: 10px;
  font-style: normal;
  font-weight: normal;
  font-size: 20px;
  line-height: 25px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
export const BottleDetailsListTitle = styled.Text`
  margin-right: 4px;
  margin-bottom: 5px;
  font-weight: 600;
  font-size: 12px;
  line-height: 16px;
  letter-spacing: -0.2px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
export const BottleDetailsAddToCartBtn = styled.TouchableOpacity`
  width: 90%;
  margin-top: 56px;
  margin-left: 5%;
  padding-vertical: 22px;
  justify-content: center;
  align-itemss: center;
  background: ${({ theme }) => theme.palette.greekAubergine};
  overflow: hidden;
  border-radius: 80px;
`;
export const BottleDetailsAddToCartText = styled.Text`
  font-size: 14px;
  line-height: 20px;
  display: flex;
  align-items: center;
  text-align: center;
  letter-spacing: -0.3px;
  color: ${({ theme }) => theme.palette.white};
`;

export const BottleDetailsFeedback = styled.View`
  background: ${({ theme }) => theme.palette.placeboOrange};
`;
export const BottleDetailsFAQTitle = styled.Text`
  margin-top: 22px;
  font-weight: normal;
  font-size: 28px;
  letter-spacing: -1px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;

export const BottleDetailsFAQSubTitle = styled.Text`
  margin-bottom: 56px;
  font-size: 20px;
  letter-spacing: -0.5px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
