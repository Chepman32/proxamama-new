import { FC } from 'react';

import styled, { css } from '@emotion/native';

import { Text } from '~/atoms/common';
import { ProductItem, ProductItemProps } from '~/molecules/catalog';

export const BottlesContainer = styled.ScrollView`
  flex: 1;
  background-color: ${({ theme }) => theme.palette.white};
`;

export const bottlesContainerContentStyle = css`
  padding-bottom: 60px;
`;

export const BottlesHeading = styled(Text)`
  font-size: 32px;
  color: ${({ theme }) => theme.palette.greekAubergine};
  margin-bottom: 35px;
`;

export const BottlesContent = styled.View`
  padding-horizontal: 24px;
`;

export const ProductItemStyled = styled<
  FC<ProductItemProps & FirstLastPositionProps>
>(ProductItem)`
  ${({ isFirst }) => !isFirst && 'margin-top: 44px;'}
`;

export const ProductItemSeparator = styled.View`
  height: 1px;
  background-color: ${({ theme }) => theme.palette.greekAubergine1};
  margin-vertical: 44px;
`;
