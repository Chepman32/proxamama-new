import React, { FC } from 'react';

import { useNavigation } from '@react-navigation/native';

import { ShopInfoMessage } from '~/molecules/shop';

import shopCategory2 from '../ShopScreen/shopCategory2.png';
import shopCategoryTypeBNipple from '../ShopScreen/shopCategoryTypeBNipple.png';

import {
  BottlesContainer,
  bottlesContainerContentStyle,
  BottlesContent,
  BottlesHeading,
  ProductItemSeparator,
  ProductItemStyled,
} from './styles';

export const BottlesScreen: FC = () => {
  const navigation = useNavigation();
  return (
    <BottlesContainer contentContainerStyle={bottlesContainerContentStyle}>
      <BottlesContent>
        <BottlesHeading>Bottles</BottlesHeading>
        <ProductItemStyled
          isFirst
          imageSource={shopCategory2}
          title="Bottle Type A + Nipple"
          price="65"
          onPress={() => navigation.navigate('BottleDetails')}
        />
        <ProductItemStyled
          imageSource={shopCategoryTypeBNipple}
          title="Bottle Type B + Nipple"
          price="65"
          onPress={() => navigation.navigate('BottleDetails')}
        />
      </BottlesContent>
      <ProductItemSeparator />
      <ShopInfoMessage
        title="Orders"
        description="Dignissim scelerisque eget nibh gravida amet, diam blandit rhoncus facilisis. Enim."
      />
      <ShopInfoMessage
        title="Subscriptions"
        description="Turpis scelerisque at egestas praesent ut elementum congue risus. Ornare."
      />
    </BottlesContainer>
  );
};
