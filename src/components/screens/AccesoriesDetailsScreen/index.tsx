import React, { FC, useState } from 'react';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';

import { useTheme } from '@emotion/react';
import { BottomCarousel } from 'components/BottomCarousel/';
import { Carousel } from 'components/Carousel';

import { Accordion } from '~/atoms/common/Accordion';
import { FeedbackItem } from '~/organisms/FeedbackItem';
import { SubscriptionDropdown } from '~/organisms/SubscriptionDropdown';

import {
  AccesoriesDetailsAddToCartBtn,
  AccesoriesDetailsAddToCartText,
  AccesoriesDetailsBottomCarousel,
  AccesoriesDetailsCarouselContainer,
  AccesoriesDetailsContainer,
  AccesoriesDetailsDesc,
  AccesoriesDetailsFAQTitle,
  AccesoriesDetailsFeedback,
  AccesoriesDetailsListTitle,
  AccesoriesDetailsPrice,
  AccesoriesDetailsQuantity,
  AccesoriesDetailsQuantityRow,
  AccesoriesDetailsSizeTitle,
  AccesoriesDetailsSubscription,
  AccesoriesDetailsSubscriptionBtn,
  AccesoriesDetailsSubscriptionPrice,
  AccesoriesDetailsSubscriptionTopRow,
  AccesoriesDetailstailsBottomTitle,
  AccesoriesDetailsTitle,
  SafeAreaViewStyled,
} from './styles';

export const AccesoriesDetailsScreen: FC = () => {
  const [subs, setSubs] = useState('');
  const [count, setCount] = useState(0);
  const { palette } = useTheme();
  const placeholder =
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';
  const quantityHandlerStyles = {
    padding: 13,
    backgroundColor: palette.placeboOrange,
    overflow: 'hidden',
    borderRadius: 24,
  };
  return (
    <SafeAreaViewStyled>
      <ScrollView>
        <AccesoriesDetailsContainer>
          <AccesoriesDetailsCarouselContainer>
            <Carousel />
          </AccesoriesDetailsCarouselContainer>
          <AccesoriesDetailsTitle>Nipple Shield</AccesoriesDetailsTitle>
          <AccesoriesDetailsPrice>$20</AccesoriesDetailsPrice>
          <AccesoriesDetailsSubscription>
            <AccesoriesDetailsSubscriptionTopRow>
              <View>
                <Text>Subscription</Text>
                <AccesoriesDetailsSubscriptionPrice>
                  $15 / mo
                </AccesoriesDetailsSubscriptionPrice>
              </View>
              <SubscriptionDropdown
                data={['week', 'month', 'year']}
                selected={subs}
                handler={setSubs}
              />
            </AccesoriesDetailsSubscriptionTopRow>
            <AccesoriesDetailsSubscriptionBtn>
              <AccesoriesDetailsAddToCartText>
                Add Subscription
              </AccesoriesDetailsAddToCartText>
            </AccesoriesDetailsSubscriptionBtn>
          </AccesoriesDetailsSubscription>
          <AccesoriesDetailsSizeTitle>Quantity</AccesoriesDetailsSizeTitle>
          <AccesoriesDetailsQuantityRow>
            <TouchableOpacity
              onPress={() => count > 0 && setCount(count - 1)}
              // @ts-ignore
              style={quantityHandlerStyles}
            >
              <AccesoriesDetailsQuantity>-</AccesoriesDetailsQuantity>
            </TouchableOpacity>
            <AccesoriesDetailsQuantity>{count} </AccesoriesDetailsQuantity>
            <TouchableOpacity
              onPress={() => setCount(count + 1)}
              // @ts-ignore
              style={quantityHandlerStyles}
            >
              <AccesoriesDetailsQuantity>+</AccesoriesDetailsQuantity>
            </TouchableOpacity>
          </AccesoriesDetailsQuantityRow>
          <AccesoriesDetailsDesc>
            Designed with baby in mind, emulait eliminates bottle refusal by
            ensuring baby can latch and control the flow of milk. Our bottles
            are free of BPA, BPS and all chemicals found in traditional baby
            bottles that are known to activate estrogen.
          </AccesoriesDetailsDesc>
          <View>
            <AccesoriesDetailsListTitle>
              What is included:
            </AccesoriesDetailsListTitle>
          </View>
          <View>
            <AccesoriesDetailsDesc>&bull; 4oz container</AccesoriesDetailsDesc>
          </View>
          <View>
            <AccesoriesDetailsDesc>&bull; Non-skid base</AccesoriesDetailsDesc>
          </View>
          <View>
            <AccesoriesDetailsDesc>&bull; Travel cap</AccesoriesDetailsDesc>
          </View>
          <View>
            <AccesoriesDetailsDesc>&bull; Care manual</AccesoriesDetailsDesc>
          </View>
        </AccesoriesDetailsContainer>
        <AccesoriesDetailsFeedback>
          <FeedbackItem
            name="Jane D."
            date="8 days ago"
            rating={4}
            text="Both my babies love this bottle. My second baby I think I bought 7-8 different bottles and this one was the one she finally took!"
          />
        </AccesoriesDetailsFeedback>
        <AccesoriesDetailsContainer>
          <AccesoriesDetailsFAQTitle>
            Frequent Asked Questions
          </AccesoriesDetailsFAQTitle>
          <Accordion
            title={'Is emulait baby bottle safe in microwave?'}
            text={placeholder}
          />
          <Accordion
            title={'Are there different nipple flow rates?'}
            text={placeholder}
          />
          <Accordion
            title={'Are your bottles anti-colic?'}
            text={placeholder}
          />
          <Accordion
            title={'Are emulait bottles exempt from BPA and phthalates?'}
            text={placeholder}
          />
          <Accordion
            title={'What do I do if my bottle leaks?'}
            text={placeholder}
          />
          <Accordion
            title={'Are there different nipple flow rates?'}
            text={placeholder}
          />
          <Accordion
            title={'Are your bottles anti-colic?'}
            text={placeholder}
          />
          <Accordion
            title={'Are emulait bottles exempt from BPA and phthalates?'}
            text={placeholder}
          />
          <Accordion
            title={'What do I do if my bottle leaks?'}
            text={placeholder}
          />
        </AccesoriesDetailsContainer>
        <AccesoriesDetailsBottomCarousel>
          <AccesoriesDetailstailsBottomTitle>
            You may also like
          </AccesoriesDetailstailsBottomTitle>
          <BottomCarousel />
        </AccesoriesDetailsBottomCarousel>
        <AccesoriesDetailsAddToCartBtn>
          <AccesoriesDetailsAddToCartText>
            Add to Cart
          </AccesoriesDetailsAddToCartText>
        </AccesoriesDetailsAddToCartBtn>
      </ScrollView>
    </SafeAreaViewStyled>
  );
};
