import React, { FC } from 'react';
import { TouchableOpacityProps } from 'react-native';

import avatar from './avatar.png';
import { AvatarContainer, ImageStyled } from './styles';

export const Avatar: FC<TouchableOpacityProps> = props => {
  return (
    <AvatarContainer {...props}>
      <ImageStyled source={avatar} />
    </AvatarContainer>
  );
};
