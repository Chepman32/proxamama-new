import styled from '@emotion/native';

export const AvatarContainer = styled.TouchableOpacity`
  width: 32px;
  height: 32px;
  border-radius: 32px;
  overflow: hidden;
`;

export const ImageStyled = styled.Image`
  width: 100%;
  height: 100%;
`;
