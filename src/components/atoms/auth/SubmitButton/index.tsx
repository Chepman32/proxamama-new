import styled from '@emotion/native';

import { Button } from '~/atoms/common';

export const SubmitButton = styled(Button)`
  height: 64px;
  ${({ disabled }) => disabled && 'background-color: grey;'}
`;
