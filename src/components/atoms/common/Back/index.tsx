import React from 'react';
import { StyleProp, ViewStyle } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { useTheme } from '@emotion/react';
import { useNavigation } from '@react-navigation/native';

import { BackContainer } from './styles';

interface Props {
  style?: StyleProp<ViewStyle>;
}

export const Back = (props: Props) => {
  const navigation = useNavigation();
  const { palette } = useTheme();
  return (
    <BackContainer {...props} onPress={navigation.goBack}>
      <Svg width={9} height={14} fill="none">
        <Path
          d="M8 13 1.886 7.76a1 1 0 0 1 0-1.52L8 1"
          stroke={palette.greekAubergine}
          strokeWidth={2}
          strokeLinejoin="round"
        />
      </Svg>
    </BackContainer>
  );
};
