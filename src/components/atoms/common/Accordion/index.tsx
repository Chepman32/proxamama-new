import React, { FC, useState } from 'react';
import { View } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { useTheme } from '@emotion/react';

import {
  AccordionContainer,
  AccordionText,
  AccordionTitle,
  AccordionTitleContainer,
} from './styles';

interface IAccordion {
  title: string;
  text?: string;
}
export const Accordion: FC<IAccordion> = ({ title, text }) => {
  const [opened, setOpened] = useState(false);
  const { palette } = useTheme();
  return (
    <AccordionContainer onPress={() => setOpened(!opened)}>
      <View>
        <AccordionTitleContainer>
          <AccordionTitle>{title} </AccordionTitle>
          <Svg
            width={9}
            style={{ transform: [{ rotate: !opened ? '-90deg' : '90deg' }] }}
            height={14}
            fill="none"
          >
            <Path
              d="M8 13 1.886 7.76a1 1 0 0 1 0-1.52L8 1"
              stroke={palette.greekAubergine}
              strokeWidth={2}
              strokeLinejoin="round"
            />
          </Svg>
        </AccordionTitleContainer>

        <AccordionText>{opened && text} </AccordionText>
      </View>
    </AccordionContainer>
  );
};
