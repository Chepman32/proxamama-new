import styled from '@emotion/native';

import { Text } from '../Text';

export const AccordionContainer = styled.TouchableOpacity`
width: 100%;
padding-vertical: 26px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center
  background-color: ${({ theme }) => theme.palette.white};
  border-bottom: 1px solid ${({ theme }) => theme.palette.placeboOrange};;
`;

export const AccordionTitle = styled(Text)`
  width: 65%;
  font-size: 14px;
  line-height: 20px;
  font-weight: 500;
  letter-spacing: -0.3px;
  color: ${({ theme }) => theme.palette.greekAubergine}; ;
`;

export const AccordionText = styled(Text)`
  width: 327px;
  margin-top: 15px;
  font-size: 12px;
  line-height: 15px;
  letter-spacing: -1px;
  color: ${({ theme }) => theme.palette.greekAubergine}; ;
`;

export const AccordionTitleContainer = styled.View`
  width: 327px;
  flex-direction: row;
  justify-content: space-between;
`;
