import styled from '@emotion/native';

import { Text } from '../Text';

export const Heading = styled(Text)`
  font-size: 32px;
  font-weight: 500;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
