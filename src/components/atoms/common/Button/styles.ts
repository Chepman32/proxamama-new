import styled from '@emotion/native';

import { Text } from '../Text';

export const ButtonContainer = styled.TouchableOpacity`
  height: 44px;
  border-radius: 80px;
  justify-content: center;
  align-items: center;
  background-color: ${({ theme }) => theme.palette.greekAubergine};
`;
export const ButtonTitle = styled(Text)`
  font-weight: 600;
  color: ${({ theme }) => theme.palette.white};
`;
