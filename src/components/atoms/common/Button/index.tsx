import React, { FC } from 'react';
import { StyleProp, TextStyle, TouchableOpacityProps } from 'react-native';

import { ButtonContainer, ButtonTitle } from './styles';

interface Props extends TouchableOpacityProps {
  textStyle?: StyleProp<TextStyle>;
  title: string;
}

export const Button: FC<Props> = ({ title, textStyle, ...otherProps }) => {
  return (
    <ButtonContainer {...otherProps}>
      <ButtonTitle style={textStyle}>{title}</ButtonTitle>
    </ButtonContainer>
  );
};
