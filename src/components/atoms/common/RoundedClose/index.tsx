import React, { FC } from 'react';
import { TouchableOpacityProps } from 'react-native';

import { Close } from '~/atoms/icons';

import { RoundedCloseContainer } from './styles';

export const RoundedClose: FC<TouchableOpacityProps> = props => {
  return (
    <RoundedCloseContainer {...props}>
      <Close iconStrokeOpacity={1} />
    </RoundedCloseContainer>
  );
};
