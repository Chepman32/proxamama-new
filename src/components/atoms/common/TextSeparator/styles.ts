import styled from '@emotion/native';

import { Text } from '../Text';

export const TextSeparatorContainer = styled.View`
  align-items: center;
  justify-content: center;
  margin-vertical: 29px;
`;

export const TextSeparatorContent = styled(Text)`
  font-size: 14px;
  font-weight: 500;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
