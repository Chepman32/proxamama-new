import React, { FC } from 'react';
import { ViewProps } from 'react-native';

import { TextSeparatorContainer, TextSeparatorContent } from './styles';

interface Props extends ViewProps {
  innerContent: string;
}

export const TextSeparator: FC<Props> = ({ innerContent, ...otherProps }) => {
  return (
    <TextSeparatorContainer {...otherProps}>
      <TextSeparatorContent>{innerContent}</TextSeparatorContent>
    </TextSeparatorContainer>
  );
};
