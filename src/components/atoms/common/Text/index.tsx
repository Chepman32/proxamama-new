import styled from '@emotion/native';

import { BASE_FONT_FAMILY } from '~/constants';

export const Text = styled.Text`
  font-family: ${BASE_FONT_FAMILY};
`;
