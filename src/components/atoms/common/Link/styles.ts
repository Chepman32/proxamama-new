import styled from '@emotion/native';

import { Text } from '../Text';

export const LinkContainer = styled.TouchableOpacity``;

export const LinkTitle = styled(Text)`
  font-size: 14px;
  font-weight: 600;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
