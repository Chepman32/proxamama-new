import React, { FC } from 'react';
import { TouchableOpacityProps } from 'react-native';

import { LinkContainer, LinkTitle } from './styles';

interface Props extends TouchableOpacityProps {
  title: string;
}

export const Link: FC<Props> = props => {
  const { title } = props;
  return (
    <LinkContainer {...props}>
      <LinkTitle>{title}</LinkTitle>
    </LinkContainer>
  );
};
