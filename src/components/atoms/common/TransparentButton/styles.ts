import styled from '@emotion/native';

import { Text } from '../Text';

export const TransparentButtonContainer = styled.TouchableOpacity``;
export const TransparentButtonTitle = styled(Text)`
  font-size: 14px;
  font-weight: bold;
  text-decoration: underline;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
