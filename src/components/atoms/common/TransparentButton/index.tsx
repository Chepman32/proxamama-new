import React, { FC } from 'react';
import { StyleProp, TouchableOpacityProps, ViewStyle } from 'react-native';

import { TransparentButtonContainer, TransparentButtonTitle } from './styles';

interface Props extends TouchableOpacityProps {
  textStyle?: StyleProp<ViewStyle>;
  title: string;
}

export const TransparentButton: FC<Props> = ({
  title,
  textStyle,
  ...otherProps
}) => {
  return (
    <TransparentButtonContainer {...otherProps}>
      <TransparentButtonTitle style={textStyle}>{title}</TransparentButtonTitle>
    </TransparentButtonContainer>
  );
};
