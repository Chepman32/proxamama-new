import React, { FC, useState } from 'react';
import { View } from 'react-native';
import Svg, { Path } from 'react-native-svg';

import { useTheme } from '@emotion/react';

import {
  FullSystemContainer,
  FullSystemItemDesc,
  FullSystemItemTitle,
  FullSystemItemSvgOpened,
  FullSystemItemSvgClosed,
} from './styles';

interface IFullSystemItem {
  title: string;
  desc: string;
  isBordered?: boolean;
}
export const FullSystemItem: FC<IFullSystemItem> = ({
  title,
  desc,
  isBordered = true,
}) => {
  const [opened, setOpened] = useState(false);
  const { palette } = useTheme();
  const containerStyles = { borderBottomWidth: isBordered ? 1 : 0 };
  const innerStyles = {
    marginBottom: 9,
    flexDirection: 'row',
    alignItems: 'center',
  };
  return (
    <FullSystemContainer
      onPress={() => setOpened(!opened)}
      style={containerStyles}
    >
      <View
        // @ts-ignore
        style={innerStyles}
      >
        <FullSystemItemTitle>{title} </FullSystemItemTitle>
        {opened ? (
          <FullSystemItemSvgOpened>
            <Svg width={6} height={12} fill="none">
              <Path
                d="M8 13 1.886 7.76a1 1 0 0 1 0-1.52L8 1"
                stroke={palette.greekAubergine}
                strokeWidth={2}
                strokeLinejoin="round"
              />
            </Svg>
          </FullSystemItemSvgOpened>
        ) : (
          <FullSystemItemSvgClosed>
            <Svg width={6} height={12} fill="none">
              <Path
                d="M8 13 1.886 7.76a1 1 0 0 1 0-1.52L8 1"
                stroke={palette.greekAubergine}
                strokeWidth={2}
                strokeLinejoin="round"
              />
            </Svg>
          </FullSystemItemSvgClosed>
        )}
      </View>
      <FullSystemItemDesc>{opened && desc} </FullSystemItemDesc>
    </FullSystemContainer>
  );
};
