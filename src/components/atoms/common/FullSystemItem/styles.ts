import styled from '@emotion/native';

export const FullSystemContainer = styled.TouchableOpacity`
  padding-horizontal: 24px;
  padding-bottom: 24px;
  flex: 1;
  align-items: center;
  border-bottom: ${isBordered => (isBordered ? 1 : 0)};
`;
export const FullSystemItemTitle = styled.Text`
  margin-right: 8px;
  font-size: 14px;
  line-height: 20px;
  letter-spacing: -0.3px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;

export const FullSystemItemDesc = styled.Text`
  font-size: 12px;
  line-height: 18px;

  letter-spacing: -0.2px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
export const FullSystemItemSvgOpened = styled.View`
  transform: rotate(90deg);
`;
export const FullSystemItemSvgClosed = styled.View`
  transform: rotate(180deg);
`;
