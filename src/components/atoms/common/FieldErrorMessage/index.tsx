import styled from '@emotion/native';

import { Text } from '../Text';

export const FieldErrorMessage = styled(Text)`
  position: absolute;
  bottom: -24px;
  left: 28px;
  font-size: 12px;
  color: ${({ theme }) => theme.palette.coquelicot};
`;
