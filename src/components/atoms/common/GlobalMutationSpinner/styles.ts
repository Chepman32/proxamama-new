import styled from '@emotion/native';

export const GlobalMutationSpinnerContainer = styled.View`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  flex: 1;
  z-index: 1;
  justify-content: center;
  align-items: center;
  background-color: ${({ theme }) => theme.palette.greekAubergine50};
`;
