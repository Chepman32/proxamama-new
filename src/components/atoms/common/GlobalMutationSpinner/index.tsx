import React, { FC } from 'react';
import Spinner from 'react-native-spinkit';
import { useIsMutating } from 'react-query';

import { useTheme } from '@emotion/react';

import { MutationKeys } from '~/constants';

import { GlobalMutationSpinnerContainer } from './styles';

export const GlobalMutationSpinner: FC = () => {
  const isGoogleMutation = useIsMutating([MutationKeys.GOOGLE_AUTH]);

  const isFacebookMutation = useIsMutating([MutationKeys.FACEBOOK_AUTH]);

  const isAppleMutation = useIsMutating([MutationKeys.APPLE_AUTH]);

  const theme = useTheme();
  return isGoogleMutation || isFacebookMutation || isAppleMutation ? (
    <GlobalMutationSpinnerContainer>
      <Spinner type="ChasingDots" color={theme.palette.white} size={60} />
    </GlobalMutationSpinnerContainer>
  ) : null;
};
