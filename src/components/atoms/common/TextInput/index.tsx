import React, { FC, memo, useMemo } from 'react';

import { css } from '@emotion/native';
import { useTheme } from '@emotion/react';

import { FieldErrorMessage } from '../FieldErrorMessage';

import {
  TextInputContainer,
  textInputLabelStyles,
  textInputStyles,
} from './styles';
import { TextInputProps } from './types';

const { FloatingLabelInput } = require('react-native-floating-label-input');

export const TextInput: FC<TextInputProps> = memo(props => {
  const { palette } = useTheme();
  const containerStyles = useMemo(
    () => css`
      padding-horizontal: 28px;
      border-radius: 80px;
      background-color: ${palette.white};
      height: 64px;
      border-width: 1px;
      border-color: ${props.errorMessage ? palette.coquelicot : palette.white};
    `,
    [props.errorMessage, palette],
  );

  const customLabelStyles = useMemo(
    () => ({
      colorFocused: palette.greekAubergine50,
      colorBlurred: palette.greekAubergine50,
    }),
    [palette],
  );

  return (
    <TextInputContainer style={props.style}>
      <FloatingLabelInput
        containerStyles={{
          ...containerStyles,
          ...props.containerStyles,
        }}
        labelStyles={textInputLabelStyles}
        customLabelStyles={customLabelStyles}
        inputStyles={textInputStyles}
        label={props.label}
        onChangeText={
          props.handleChange && props.fieldName
            ? props.handleChange(props.fieldName)
            : props.onChangeText
        }
        autoCapitalize={props.autoCapitalize}
        isPassword={props.isPassword}
        value={props.value}
      />
      {props.errorMessage && (
        <FieldErrorMessage style={props.customErrorMessageStyle}>
          {props.errorMessage}
        </FieldErrorMessage>
      )}
    </TextInputContainer>
  );
});
