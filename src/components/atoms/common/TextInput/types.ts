import { StyleProp, TextStyle } from 'react-native';

export interface TextInputProps extends FloatingLabelInputProps {
  fieldName?: string;
  errorMessage?: string;
  customErrorMessageStyle?: StyleProp<TextStyle>;
  handleChange?: {
    (e: React.ChangeEvent<any>): void;
    <T = string | React.ChangeEvent<any>>(
      field: T,
    ): T extends React.ChangeEvent<any>
      ? void
      : (e: string | React.ChangeEvent<any>) => void;
  };
}
