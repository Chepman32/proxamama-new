import styled, { css } from '@emotion/native';

import { BASE_FONT_FAMILY } from '~/constants';

export const textInputStyles = css`
  padding-horizontal: 5px;
`;

export const TextInputContainer = styled.View``;

export const textInputLabelStyles = css`
  font-family: ${BASE_FONT_FAMILY};
`;
