import React, { FC, useMemo, useState } from 'react';
import { StyleProp, TouchableOpacityProps, ViewStyle } from 'react-native';

import { Arrow } from '~/atoms/icons';

import {
  DrawerMenuItemContainer,
  DrawerMenuItemSubParagraphs,
  DrawerMenuItemText,
} from './styles';

interface Props extends TouchableOpacityProps, FirstLastPositionProps {
  title: string;
  small?: boolean;
}

export const DrawerMenuItem: FC<Props> = ({
  title,
  small,
  children,
  ...props
}) => {
  const [visible, setVisible] = useState(false);

  const arrowStyle: StyleProp<ViewStyle> = useMemo(
    () => ({
      transform: [{ rotate: visible ? '0deg' : '-180deg' }],
      marginRight: 6,
    }),
    [visible],
  );

  return (
    <>
      <DrawerMenuItemContainer onPress={() => setVisible(s => !s)} {...props}>
        <DrawerMenuItemText small={small}>{title}</DrawerMenuItemText>
        {children ? <Arrow style={arrowStyle} /> : null}
      </DrawerMenuItemContainer>
      {children && visible ? (
        <DrawerMenuItemSubParagraphs>{children}</DrawerMenuItemSubParagraphs>
      ) : null}
      {}
    </>
  );
};
