import styled from '@emotion/native';

import { Text } from '~/atoms/common';

export const DrawerMenuItemContainer = styled.TouchableOpacity<
  FirstLastPositionProps & {
    hideBorderBottom?: boolean;
  }
>`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  ${({ isFirst, hideBorderBottom, theme }) =>
    !isFirst && !hideBorderBottom
      ? `border-top-width: 1px; border-color: ${theme.palette.springSong15};`
      : ''}
  padding-vertical: 20px;
`;

export const DrawerMenuItemText = styled(Text)<{ small?: boolean }>`
  ${({ small }) =>
    small
      ? 'font-size: 18px; font-weight: 400;'
      : 'font-size: 32px; font-weight: 500;'}
  color: ${({ theme }) => theme.palette.springSong};
`;

export const DrawerMenuItemSubParagraphs = styled.View`
  margin-vertical: -8px;
  padding-bottom: 20px;
`;
