import React, { FC } from 'react';
import { TouchableOpacityProps } from 'react-native';

import {
  DrawerMenuItemSubParagraphContainer,
  DrawerMenuItemSubParagraphText,
} from './styles';

interface Props extends TouchableOpacityProps {
  title: string;
}

export const DrawerMenuItemSubParagraph: FC<Props> = ({ title, ...props }) => {
  return (
    <DrawerMenuItemSubParagraphContainer {...props}>
      <DrawerMenuItemSubParagraphText>{title}</DrawerMenuItemSubParagraphText>
    </DrawerMenuItemSubParagraphContainer>
  );
};
