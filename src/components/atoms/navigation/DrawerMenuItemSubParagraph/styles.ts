import styled from '@emotion/native';

import { Text } from '~/atoms/common';

export const DrawerMenuItemSubParagraphContainer = styled.TouchableOpacity`
  padding-vertical: 8px;
`;

export const DrawerMenuItemSubParagraphText = styled(Text)`
  font-size: 20px;
  color: ${({ theme }) => theme.palette.springSong};
`;
