import React, { FC, useEffect, useRef } from 'react';

import { BottomSheetModalMethods } from '@gorhom/bottom-sheet/lib/typescript/types';

import { CartBottomSheetModal } from '~/organisms/cart';

import { CartButtonCounter } from './CartButtonCounter';
import { CartButtonContainer, CartButtonLabel } from './styles';

export const CartButton: FC = () => {
  const bottomSheetModalRef = useRef<BottomSheetModalMethods>(null);
  useEffect(() => {
    bottomSheetModalRef.current?.present();
  }, []);
  return (
    <>
      <CartButtonContainer
        onPress={() => {
          bottomSheetModalRef.current?.expand();
        }}
      >
        <CartButtonCounter count={0} />
        <CartButtonLabel>Cart</CartButtonLabel>
      </CartButtonContainer>
      <CartBottomSheetModal
        ref={bottomSheetModalRef}
        onDismiss={() => bottomSheetModalRef.current?.present()}
      />
    </>
  );
};
