import { TouchableOpacity } from 'react-native-gesture-handler';

import styled from '@emotion/native';

import { Text } from '~/atoms/common';

export const CartButtonContainer = styled(TouchableOpacity)`
  flex-direction: row;
  align-items: center;
  padding: 6px 14px 6px 8px;
  background-color: ${({ theme }) => theme.palette.greekAubergine};
  border-radius: 40px;
`;

export const CartButtonLabel = styled(Text)`
  font-size: 12px;
  font-weight: bold;
  color: ${({ theme }) => theme.palette.placeboOrange};
  margin-left: 8px;
`;
