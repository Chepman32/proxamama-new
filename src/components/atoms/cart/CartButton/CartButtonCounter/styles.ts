import styled from '@emotion/native';

import { Text } from '~/atoms/common';

export const CartButtonCounterContainer = styled.View`
  width: 28px;
  height: 28px;
  border-radius: 28px;
  justify-content: center;
  align-items: center;
  background-color: ${({ theme }) => theme.palette.placeboOrange};
`;

export const CartButtonCounterTitle = styled(Text)`
  font-size: 14px;
  font-weight: bold;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
