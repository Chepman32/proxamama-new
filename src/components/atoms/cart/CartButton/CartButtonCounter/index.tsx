import React, { FC } from 'react';

import { CartButtonCounterContainer, CartButtonCounterTitle } from './styles';

interface Props {
  count: number;
}

export const CartButtonCounter: FC<Props> = ({ count }) => (
  <CartButtonCounterContainer>
    <CartButtonCounterTitle>{count}</CartButtonCounterTitle>
  </CartButtonCounterContainer>
);
