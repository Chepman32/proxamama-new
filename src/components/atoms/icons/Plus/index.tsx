import React from 'react';
import Svg, { Path, SvgProps } from 'react-native-svg';

export const Plus = (props: SvgProps) => (
  <Svg width={12} height={12} fill="none" {...props}>
    <Path
      d="M6.041.284V6.04m-.04 5.674.04-5.674m0 0H.286m5.755 0h5.674"
      stroke="#3F0733"
      strokeWidth={2}
    />
  </Svg>
);
