import React from 'react';
import Svg, { Path, SvgProps } from 'react-native-svg';

export const Google = (props: SvgProps) => (
  <Svg width={20} height={20} fill="none" {...props}>
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M20 10.227c0-.709-.065-1.39-.186-2.045h-9.61v3.868h5.492c-.237 1.25-.956 2.31-2.037 3.018v2.51h3.298c1.93-1.742 3.043-4.305 3.043-7.35Z"
      fill="#4285F4"
    />
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M10.204 20c2.755 0 5.065-.895 6.753-2.423l-3.298-2.509c-.914.6-2.082.955-3.455.955-2.658 0-4.908-1.76-5.71-4.123h-3.41v2.59c1.68 3.27 5.13 5.51 9.12 5.51Z"
      fill="#34A853"
    />
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M4.494 11.9c-.204-.6-.32-1.24-.32-1.9 0-.66.116-1.3.32-1.9V5.509H1.085a9.834 9.834 0 0 0 0 8.982l3.41-2.591Z"
      fill="#FBBC05"
    />
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M10.204 3.977c1.498 0 2.843.505 3.9 1.496l2.927-2.868C15.264.99 12.954 0 10.204 0c-3.99 0-7.44 2.24-9.12 5.51l3.41 2.59c.802-2.364 3.052-4.123 5.71-4.123Z"
      fill="#EA4335"
    />
  </Svg>
);
