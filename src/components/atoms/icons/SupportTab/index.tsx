import React from 'react';
import { Path } from 'react-native-svg';

export const SupportTab = () => (
  <>
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M9.333 5.835a7.584 7.584 0 0 0 0 15.122v-2.343a5.25 5.25 0 0 1 0-10.436V5.835Z"
      fill="#FB825B"
    />
    <Path
      d="M11.667 6.978h6.416a6.417 6.417 0 1 1 0 12.833H14.68a.21.21 0 0 0-.159.072L10.5 24.5"
      stroke="#FB825B"
      strokeWidth={2.333}
    />
  </>
);
