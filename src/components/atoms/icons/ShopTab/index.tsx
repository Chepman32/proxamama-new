import React from 'react';
import { Path } from 'react-native-svg';

export const ShopTab = () => (
  <>
    <Path
      d="M5.833 9.334v10.5a3.5 3.5 0 0 0 3.5 3.5h9.334a3.5 3.5 0 0 0 3.5-3.5v-10.5M17.5 7.001a3.5 3.5 0 1 0-7 0"
      stroke="#FB825B"
      strokeWidth={2.333}
    />
    <Path
      d="M10.5 7v2.333M17.5 7v2.333"
      stroke="#FB825B"
      strokeWidth={2.333}
      strokeLinejoin="round"
    />
  </>
);
