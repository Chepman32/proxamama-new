import React, { memo } from 'react';
import { TouchableOpacityProps } from 'react-native';
import Svg, { G } from 'react-native-svg';

import { NavigationProp, useNavigation } from '@react-navigation/native';

import { BabyTab } from '../BabyTab';
import { ShopTab } from '../ShopTab';
import { SupportTab } from '../SupportTab';

import { TabIconContainer } from './styles';

interface TabIconProps extends TouchableOpacityProps {
  active?: boolean;
  tabRouteName: keyof TabNavigatorParamList;
}

export const TabIcon = memo(
  ({ active, tabRouteName, ...props }: TabIconProps) => {
    const navigation = useNavigation<NavigationProp<TabNavigatorParamList>>();
    return (
      <TabIconContainer
        onPress={() => navigation.navigate(tabRouteName)}
        {...props}
      >
        <Svg width={28} height={28} fill="none">
          <G opacity={active ? 1 : 0.3}>
            {tabRouteName === 'Baby' ? <BabyTab /> : null}
            {tabRouteName === 'ShopStackNavigator' ? <ShopTab /> : null}
            {tabRouteName === 'Support' ? <SupportTab /> : null}
          </G>
        </Svg>
      </TabIconContainer>
    );
  },
);
