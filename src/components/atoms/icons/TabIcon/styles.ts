import styled from '@emotion/native';

export const TabIconContainer = styled.TouchableOpacity`
  flex-grow: 1;
  justify-content: center;
  align-items: center;
`;
