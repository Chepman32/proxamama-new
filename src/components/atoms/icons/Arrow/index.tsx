import React from 'react';
import Svg, { Path, SvgProps } from 'react-native-svg';

export const Arrow = (props: SvgProps) => (
  <Svg width={14} height={9} fill="none" {...props}>
    <Path
      d="m1 8 5.24-6.114a1 1 0 0 1 1.52 0L13 8"
      stroke="#FCCCBF"
      strokeWidth={2}
      strokeLinejoin="round"
    />
  </Svg>
);
