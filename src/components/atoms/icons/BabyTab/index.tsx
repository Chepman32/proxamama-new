import React from 'react';
import { Path } from 'react-native-svg';

export const BabyTab = () => (
  <>
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M21.545 10.875a8.166 8.166 0 1 1-13.378-2.59V5.269a10.5 10.5 0 1 0 4.666-1.704v2.352a8.168 8.168 0 0 1 8.712 4.958Z"
      fill="#FB825B"
    />
    <Path
      d="M17.5 14a3.5 3.5 0 1 1-7 0M14 9.333a2.333 2.333 0 0 1 0-4.666"
      stroke="#FB825B"
      strokeWidth={2.333}
    />
  </>
);
