import React from 'react';
import Svg, { Path, SvgProps } from 'react-native-svg';

export const Minus = (props: SvgProps) => (
  <Svg width={12} height={3} fill="none" {...props}>
    <Path d="M.286 1.041h11.429" stroke="#3F0733" strokeWidth={2} />
  </Svg>
);
