import React from 'react';
import Svg, { Path, SvgProps } from 'react-native-svg';

interface Props extends SvgProps {
  iconStrokeOpacity?: number;
}

export const Close = ({ iconStrokeOpacity = 0.5, ...props }: Props) => (
  <Svg width={14} height={14} fill="none" {...props}>
    <Path
      d="M13.104.981 7 7.085M.94 13.06 7 7.085m0 0L.896.981M7 7.085l6.018 6.018"
      stroke="#3F0733"
      strokeOpacity={iconStrokeOpacity}
      strokeWidth={2.333}
    />
  </Svg>
);
