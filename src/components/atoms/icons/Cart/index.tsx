import React from 'react';
import Svg, { G, Path, SvgProps } from 'react-native-svg';

export const Cart = (props: SvgProps) => (
  <Svg width={49} height={48} fill="none" {...props}>
    <G opacity={0.5} stroke="#3F0733" strokeWidth={4}>
      <Path d="M10.5 16v18a6 6 0 0 0 6 6h16a6 6 0 0 0 6-6V16M30.5 12a6 6 0 1 0-12 0" />
      <Path d="M18.5 12v4M30.5 12v4" strokeLinejoin="round" />
    </G>
  </Svg>
);
