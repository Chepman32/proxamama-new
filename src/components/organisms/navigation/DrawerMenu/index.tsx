import React, { FC, useEffect } from 'react';
import { StatusBar } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useDispatch } from 'react-redux';

import {
  DrawerContentComponentProps,
  useDrawerStatus,
} from '@react-navigation/drawer';
import { useNavigation } from '@react-navigation/native';

import { DrawerMenuItem, DrawerMenuItemSubParagraph } from '~/atoms/navigation';

import {
  AvatarStyled,
  DrawerMenuContainer,
  DrawerMenuRightRectangle,
  DrawerMenuRightRectangleInner,
  DrawerMenuRightRectangleWrapper,
} from './styles';

export const DrawerMenu: FC<DrawerContentComponentProps> = () => {
  const drawerStatus = useDrawerStatus();
  const navigation = useNavigation();
  const { authFlow } = useDispatch();
  useEffect(() => {
    StatusBar.setBarStyle(
      drawerStatus === 'open' ? 'light-content' : 'dark-content',
    );
  }, [drawerStatus]);

  return (
    <>
      <SafeAreaView edges={['top', 'bottom']}>
        <DrawerMenuContainer>
          <AvatarStyled />
          <DrawerMenuItem title="Activity" isFirst />
          <DrawerMenuItem title="Shop">
            <DrawerMenuItemSubParagraph
              title="Full System"
              onPress={() => navigation.navigate('FullSystem')}
            />
            <DrawerMenuItemSubParagraph
              title="Add-On Products"
              onPress={() => navigation.navigate('Addons')}
            />
            <DrawerMenuItemSubParagraph
              title="Full Bottle"
              onPress={() => navigation.navigate('BottleDetails')}
            />
          </DrawerMenuItem>
          <DrawerMenuItem
            title="Support"
            onPress={() => navigation.navigate('Support')}
          />
          <DrawerMenuItem
            title="Log out"
            onPress={authFlow.signout}
            small
            isLast
          />
        </DrawerMenuContainer>
      </SafeAreaView>
      <DrawerMenuRightRectangleWrapper>
        <DrawerMenuRightRectangle>
          <DrawerMenuRightRectangleInner />
        </DrawerMenuRightRectangle>
      </DrawerMenuRightRectangleWrapper>
    </>
  );
};
