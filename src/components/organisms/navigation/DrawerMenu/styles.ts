import { Dimensions } from 'react-native';

import styled from '@emotion/native';

import { Avatar } from '~/atoms/auth';

export const DrawerMenuContainer = styled.View`
  padding-top: 20px;
  padding-right: 44px;
  padding-bottom: 20px;
  padding-left: 24px;
`;

export const DrawerMenuRightRectangleWrapper = styled.View`
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  height: ${Dimensions.get('screen').height.toString()}px;
  overflow: hidden;
  justify-content: center;
  align-items: center;
`;

export const DrawerMenuRightRectangle = styled.View`
  margin-right: -20px;
  width: 40px;
  height: 80%;
  justify-content: center;
  align-items: center;
  background-color: ${({ theme }) => theme.palette.white};
  border-radius: 30px;
`;

export const DrawerMenuRightRectangleInner = styled.View`
  width: 4px;
  height: 32px;
  margin-right: 20px;
  background-color: ${({ theme }) => theme.palette.greekAubergine};
  opacity: 0.1;
  border-radius: 8px;
`;

export const AvatarStyled = styled(Avatar)`
  width: 64px;
  height: 64px;
  margin-bottom: 12px;
`;
