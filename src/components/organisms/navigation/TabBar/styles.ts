import { SafeAreaView } from 'react-native-safe-area-context';

import styled from '@emotion/native';

export const SafeAreaViewStyled = styled(SafeAreaView)`
  background-color: ${({ theme }) => theme.palette.white};
`;

export const TabBarContainer = styled.View`
  flex-direction: row;
  height: 56px;
`;
