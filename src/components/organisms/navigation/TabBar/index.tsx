import React, { FC } from 'react';

import { BottomTabBarProps } from '@react-navigation/bottom-tabs';

import { TabIcon } from '~/atoms/icons';

import { SafeAreaViewStyled, TabBarContainer } from './styles';

export const TabBar: FC<BottomTabBarProps> = props => (
  <SafeAreaViewStyled mode="padding" edges={['bottom']}>
    <TabBarContainer>
      {props.state.routes.map((item, index) => {
        return (
          <TabIcon
            tabRouteName={item.name as keyof TabNavigatorParamList}
            active={props.state.index === index}
            key={item.key}
          />
        );
      })}
    </TabBarContainer>
  </SafeAreaViewStyled>
);
