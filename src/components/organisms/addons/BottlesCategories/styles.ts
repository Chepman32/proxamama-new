import styled from '@emotion/native';

import { Text } from '~/atoms/common';

export const BottlesCategoriesContainer = styled.View``;

export const BottlesCategoriesHeading = styled(Text)`
  font-size: 20px;
  color: ${({ theme }) => theme.palette.velvetCosmos};
`;
