import React, { FC } from 'react';

import { useNavigation } from '@react-navigation/native';

import bottleAddons1 from '~/assets/images/addons/bottleAddons/1.png';
import bottleAddons2 from '~/assets/images/addons/bottleAddons/2.png';
import bundles1 from '~/assets/images/addons/bundles/1.png';
import cleaning1 from '~/assets/images/addons/cleaning/1.png';
import cleaning2 from '~/assets/images/addons/cleaning/2.png';
import cleaning3 from '~/assets/images/addons/cleaning/3.png';
import nursingFeeding1 from '~/assets/images/addons/nursingFeeding/1.png';
import nursingFeeding2 from '~/assets/images/addons/nursingFeeding/2.png';
import nursingFeeding3 from '~/assets/images/addons/nursingFeeding/3.png';
import nursingFeeding4 from '~/assets/images/addons/nursingFeeding/4.png';
import nursingFeeding5 from '~/assets/images/addons/nursingFeeding/5.png';
import transport1 from '~/assets/images/addons/transport/1.png';
import transport2 from '~/assets/images/addons/transport/2.png';

import {
  AddonsCategoriesContainer,
  AddonsCategoriesHeading,
  AddonsCategoriesList,
  productItemImageStyle,
  productItemPriceStyle,
  ProductItemStyled,
  productItemTitleStyle,
} from './styles';

interface Props {}

export const AddonsCategories: FC<Props> = () => {
  const navigation = useNavigation();
  return (
    <AddonsCategoriesContainer>
      <AddonsCategoriesHeading>Bottle Add-Ons</AddonsCategoriesHeading>
      <AddonsCategoriesList>
        <ProductItemStyled
          imageSource={bottleAddons1}
          title="Nipple Shield"
          price="10"
          imageStyle={productItemImageStyle}
          titleStyle={productItemTitleStyle}
          priceStyle={productItemPriceStyle}
          onPress={() => navigation.navigate('AccessoriesDetails')}
        />
        <ProductItemStyled
          imageSource={bottleAddons2}
          title="Base + Handles"
          price="30"
          imageStyle={productItemImageStyle}
          titleStyle={productItemTitleStyle}
          priceStyle={productItemPriceStyle}
          onPress={() => navigation.navigate('AccessoriesDetails')}
        />
      </AddonsCategoriesList>
      <AddonsCategoriesHeading>Nursing & Feeding</AddonsCategoriesHeading>
      <AddonsCategoriesList>
        <ProductItemStyled
          imageSource={nursingFeeding1}
          title="Bottle Warmer"
          price="40"
          imageStyle={productItemImageStyle}
          titleStyle={productItemTitleStyle}
          priceStyle={productItemPriceStyle}
          onPress={() => navigation.navigate('AccessoriesDetails')}
        />
        <ProductItemStyled
          imageSource={nursingFeeding2}
          title="Formula Maker"
          price="80"
          imageStyle={productItemImageStyle}
          titleStyle={productItemTitleStyle}
          priceStyle={productItemPriceStyle}
          onPress={() => navigation.navigate('AccessoriesDetails')}
        />
        <ProductItemStyled
          imageSource={nursingFeeding3}
          title="Pacifiers"
          price="10"
          imageStyle={productItemImageStyle}
          titleStyle={productItemTitleStyle}
          priceStyle={productItemPriceStyle}
          onPress={() => navigation.navigate('AccessoriesDetails')}
        />
        <ProductItemStyled
          imageSource={nursingFeeding4}
          title="Breast Pump"
          price="80"
          imageStyle={productItemImageStyle}
          titleStyle={productItemTitleStyle}
          priceStyle={productItemPriceStyle}
          onPress={() => navigation.navigate('AccessoriesDetails')}
        />
        <ProductItemStyled
          imageSource={nursingFeeding5}
          title="Nursing Pads"
          price="20"
          imageStyle={productItemImageStyle}
          titleStyle={productItemTitleStyle}
          priceStyle={productItemPriceStyle}
          onPress={() => navigation.navigate('AccessoriesDetails')}
        />
      </AddonsCategoriesList>
      <AddonsCategoriesHeading>Transport</AddonsCategoriesHeading>
      <AddonsCategoriesList>
        <ProductItemStyled
          imageSource={transport1}
          title="Travel Cooler Bag"
          price="150"
          imageStyle={productItemImageStyle}
          titleStyle={productItemTitleStyle}
          priceStyle={productItemPriceStyle}
          onPress={() => navigation.navigate('AccessoriesDetails')}
        />
        <ProductItemStyled
          imageSource={transport2}
          title="Milk Storage Bag"
          price="40"
          imageStyle={productItemImageStyle}
          titleStyle={productItemTitleStyle}
          priceStyle={productItemPriceStyle}
          onPress={() => navigation.navigate('AccessoriesDetails')}
        />
      </AddonsCategoriesList>
      <AddonsCategoriesHeading>Cleaning</AddonsCategoriesHeading>
      <AddonsCategoriesList>
        <ProductItemStyled
          imageSource={cleaning1}
          title="Bottle Drying Rack"
          price="40"
          imageStyle={productItemImageStyle}
          titleStyle={productItemTitleStyle}
          priceStyle={productItemPriceStyle}
          onPress={() => navigation.navigate('AccessoriesDetails')}
        />
        <ProductItemStyled
          imageSource={cleaning2}
          title="Microwave Bottle Steam Sanitizer"
          price="90"
          imageStyle={productItemImageStyle}
          titleStyle={productItemTitleStyle}
          priceStyle={productItemPriceStyle}
          onPress={() => navigation.navigate('AccessoriesDetails')}
        />
        <ProductItemStyled
          imageSource={cleaning3}
          title="Dishwasher Basket"
          price="30"
          imageStyle={productItemImageStyle}
          titleStyle={productItemTitleStyle}
          priceStyle={productItemPriceStyle}
          onPress={() => navigation.navigate('AccessoriesDetails')}
        />
      </AddonsCategoriesList>
      <AddonsCategoriesHeading>Bundles</AddonsCategoriesHeading>
      <AddonsCategoriesList>
        <ProductItemStyled
          imageSource={bundles1}
          title="Toddler Two Pack "
          price="110"
          imageStyle={productItemImageStyle}
          titleStyle={productItemTitleStyle}
          priceStyle={productItemPriceStyle}
          onPress={() => navigation.navigate('AccessoriesDetails')}
        />
      </AddonsCategoriesList>
    </AddonsCategoriesContainer>
  );
};
