import { ImageStyle, Text, TextStyle } from 'react-native';

import styled, { css } from '@emotion/native';

import { ProductItem } from '~/molecules/catalog';

export const AddonsCategoriesContainer = styled.View``;

export const AddonsCategoriesHeading = styled(Text)`
  font-size: 20px;
  font-weight: 500;
  color: ${({ theme }) => theme.palette.velvetCosmos};
  margin-bottom: 32px;
  padding-horizontal: 24px;
`;

export const AddonsCategoriesList = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  padding-horizontal: 14px;
  margin-top: -32px;
  margin-bottom: 56px;
`;

export const ProductItemStyled = styled(ProductItem)`
  width: 50%;
  padding-horizontal: 10px;
  margin-top: 32px;
`;

export const productItemImageStyle = css<ImageStyle>`
  height: 172px;
`;

export const productItemTitleStyle = css<TextStyle>`
  font-size: 14px;
`;

export const productItemPriceStyle = css<TextStyle>`
  font-size: 14px;
`;
