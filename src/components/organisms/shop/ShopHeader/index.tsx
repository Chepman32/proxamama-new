import React, { FC } from 'react';
import { NativeModules } from 'react-native';

import Bottle from './Bottle.jpeg';
import {
  ButtonStyled,
  ShopHeaderContainer,
  ShopHeaderDescription,
  ShopHeaderImage,
  ShopHeaderImageContainer,
  ShopHeaderTitle,
} from './styles';

export const ShopHeader: FC = () => {
  return (
    <ShopHeaderContainer>
      <ShopHeaderTitle>Shop</ShopHeaderTitle>
      <ShopHeaderDescription>
        Bottles tailored for you, by you.
      </ShopHeaderDescription>
      <ShopHeaderImageContainer>
        <ShopHeaderImage source={Bottle} resizeMode="contain" />
      </ShopHeaderImageContainer>
      <ButtonStyled
        title="Customize Bottle"
        onPress={() => NativeModules.RNTScanner.showScan()}
      />
    </ShopHeaderContainer>
  );
};
