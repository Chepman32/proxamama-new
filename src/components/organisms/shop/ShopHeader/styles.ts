import styled from '@emotion/native';

import { Button, Text } from '~/atoms/common';

export const ShopHeaderContainer = styled.View`
  padding-horizontal: 24px;
  padding-bottom: 24px;
`;

export const ShopHeaderTitle = styled(Text)`
  font-size: 32px;
  font-weight: 500;
  color: ${({ theme }) => theme.palette.velvetCosmos};
`;

export const ShopHeaderDescription = styled(Text)`
  font-size: 14px;
  color: ${({ theme }) => theme.palette.velvetCosmos};
  margin-top: 12px;
`;

export const ShopHeaderImageContainer = styled.View`
  align-items: center;
  margin-top: 33px;
  margin-bottom: 15px;
`;

export const ShopHeaderImage = styled.Image`
  height: 276px;
`;

export const ButtonStyled = styled(Button)`
  height: 64px;
  padding-horizontal: 32px;
  align-self: center;
`;
