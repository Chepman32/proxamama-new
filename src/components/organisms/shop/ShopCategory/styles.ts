import styled from '@emotion/native';

import { Button, Text } from '~/atoms/common';

export const ShopCategoryContainer = styled.View`
  padding-horizontal: 24px;
`;

export const ShopCategoryTitle = styled(Text)`
  font-size: 28px;
  font-weight: 500;
  margin-bottom: 15px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;

export const ShopCategoryImage = styled.Image`
  max-width: 100%;
  height: 400px;
  border-radius: 16px;
`;

export const ShopCategoryDescription = styled(Text)`
  margin-vertical: 24px;
  font-size: 16px;
  line-height: 24px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;

export const ShopCategoryActionButton = styled(Button)`
  height: 64px;
`;
