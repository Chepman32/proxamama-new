import React, { FC } from 'react';

import {
  ShopCategoryActionButton,
  ShopCategoryContainer,
  ShopCategoryDescription,
  ShopCategoryImage,
  ShopCategoryTitle,
} from './styles';
import { ShopCategoryProps } from './types';

export const ShopCategory: FC<ShopCategoryProps> = ({
  title,
  description,
  buttonTitle,
  imageSource,
  style,
  onPress,
}) => {
  return (
    <ShopCategoryContainer style={style}>
      <ShopCategoryTitle>{title}</ShopCategoryTitle>
      <ShopCategoryImage source={imageSource} resizeMode="cover" />
      <ShopCategoryDescription>{description}</ShopCategoryDescription>
      <ShopCategoryActionButton onPress={onPress} title={buttonTitle} />
    </ShopCategoryContainer>
  );
};
