import {
  GestureResponderEvent,
  ImageSourcePropType,
  StyleProp,
  ViewStyle,
} from 'react-native';

export interface ShopCategoryProps {
  title: string;
  description: string;
  imageSource: ImageSourcePropType;
  buttonTitle: string;
  onPress?: ((event: GestureResponderEvent) => void) | undefined;
  style?: StyleProp<ViewStyle>;
}
