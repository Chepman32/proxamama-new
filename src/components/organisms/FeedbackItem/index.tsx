import React, { FC } from 'react';
import Svg, { Path } from 'react-native-svg';

import { useTheme } from '@emotion/react';

import { Star } from '../common/svg';

import {
  FeedbackItemArrow,
  FeedbackItemBody,
  FeedbackItemControls,
  FeedbackItemDate,
  FeedbackItemName,
  FeedbackItemRating,
  FeedbackItemText,
  FeedbackItemTopRow,
} from './styles';

interface IFeedbackItem {
  name: string;
  date: string;
  rating: number;
  text: string;
}
export const FeedbackItem: FC<IFeedbackItem> = ({
  name,
  date,
  rating,
  text,
}) => {
  const { palette } = useTheme();
  const renderRating = () => {
    const stars = [];
    for (let i = 0; i < rating; i++) {
      stars.push(<Star key={i.toString()} />);
    }
    return stars;
  };
  return (
    <FeedbackItemBody>
      <FeedbackItemTopRow>
        <FeedbackItemName>{name} </FeedbackItemName>
        <FeedbackItemDate>{date} </FeedbackItemDate>
      </FeedbackItemTopRow>
      <FeedbackItemRating>{renderRating()}</FeedbackItemRating>
      <FeedbackItemText>{text}</FeedbackItemText>
      <FeedbackItemControls>
        <FeedbackItemArrow>
          <Svg width={9} height={14} fill="none">
            <Path
              d="M8 13 1.886 7.76a1 1 0 0 1 0-1.52L8 1"
              stroke={palette.greekAubergine}
              strokeWidth={2}
              strokeLinejoin="round"
            />
          </Svg>
        </FeedbackItemArrow>
        <FeedbackItemArrow>
          <Svg
            width={9}
            style={{ transform: [{ rotate: '180deg' }] }}
            height={14}
            fill="none"
          >
            <Path
              d="M8 13 1.886 7.76a1 1 0 0 1 0-1.52L8 1"
              stroke={palette.greekAubergine}
              strokeWidth={2}
              strokeLinejoin="round"
            />
          </Svg>
        </FeedbackItemArrow>
      </FeedbackItemControls>
    </FeedbackItemBody>
  );
};
