import styled from '@emotion/native';
export const FeedbackItemBody = styled.View`
  padding-vertical: 40px;
  padding-horizontal: 24px;
`;
export const FeedbackItemTopRow = styled.View`
  margin-bottom: 32px;
  flex-direction: row;
  justify-content: space-between;
`;

export const FeedbackItemName = styled.Text`
  font-size: 12px;
  line-height: 16px;
  letter-spacing: -0.2px;
  color: #3f0733;
`;

export const FeedbackItemDate = styled.Text`
  font-size: 12px;
  line-height: 16px;
  text-align: right;
  letter-spacing: -0.2px;
  color: #3f0733;
  opacity: 0.6;
`;

export const FeedbackItemRating = styled.View`
  margin-top: 32px;
  margin-bottom: 29px;
  flex-direction: row;
  justify-content: flex-start;
`;

export const FeedbackItemText = styled.Text`
  font-size: 14px;
  line-height: 20px;
  letter-spacing: -0.3px;
  color: #3f0733;
`;

export const FeedbackItemControls = styled.View`
  margin-top: 53px;
  padding-vertical: 40px;
  padding-horizontal: 24px;
  margin-bottom: 32px;
  flex-direction: row;
  justify-content: flex-start;
`;

export const FeedbackItemArrow = styled.TouchableOpacity`
  margin-right: 16px;
  padding: 12px;
  background: #ffffff;
  border-radius: 24px;
`;
