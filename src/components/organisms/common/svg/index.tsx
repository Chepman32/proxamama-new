import React from 'react';
import { Path, Svg } from 'react-native-svg';

export const Star = () => (
  <Svg width={18} height={17} fill="none">
    <Path
      d="m8.721 0 2.343 5.633 6.082.488-4.634 3.969 1.416 5.934-5.207-3.18-5.206 3.18L4.93 10.09.297 6.12l6.081-.487L8.721 0Z"
      fill="#3F0733"
    />
  </Svg>
);
