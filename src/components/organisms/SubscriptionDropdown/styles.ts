import styled from '@emotion/native';

export const DropdownContainer = styled.TouchableOpacity`
  width: 100%;
  padding-vertical: 18px;
  padding-right: 30px;
  padding-left: 24px;
  flex-direction: row;
  overflow: hidden;
  font-size: 44px;
  line-height: 20px;
  letter-spacing: -0.3px;
  color: ${({ theme }) => theme.palette.white};
`;
export const DropdownText = styled.Text`
  padding-right: 14px;
  font-size: 14px;
  line-height: 20px;
  letter-spacing: -0.3px;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
