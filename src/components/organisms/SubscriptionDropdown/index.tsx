import React, { FC, useState } from 'react';
import { View } from 'react-native';
// @ts-ignore
import ModalDropdown from 'react-native-modal-dropdown';
import Svg, { Path } from 'react-native-svg';

import styled from '@emotion/native';
import { useTheme } from '@emotion/react';

import { DropdownContainer, DropdownText } from './styles';

interface ISubscriptionDropdown {
  data: string[];
  handler: any;
  selected: string;
}
export const SubscriptionDropdown: FC<ISubscriptionDropdown> = ({
  data,
  handler,
  selected,
}) => {
  const [isOpened, setIsOpened] = useState(false);
  const renderRow = (rowData: string, rowID: number) => {
    const dropdownContainerStyles = {
      justifyContent: 'space-between',
      overflow: 'hidden',
      borderBottomStartRadius: rowID === data.length ? 24 : 0,
      borderBottomEndRadius: rowID === data.length ? 24 : 0,

      opacity: selected === rowData ? 0.2 : 1,
    };
    return (
      // @ts-ignore
      <DropdownContainer
        onPress={() => handler(rowData)}
        // @ts-ignore
        style={dropdownContainerStyles}
      >
        <DropdownText>{rowData} </DropdownText>
        <ArrowContainer />
      </DropdownContainer>
    );
  };
  const { palette } = useTheme();
  const dropDownstyles = {
    width: 156,
    paddingVertical: 12,
    paddingLeft: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: palette.white,
    overflow: 'hidden',
    borderWidth: 1,
    borderRadius: 24,
    borderColor: palette.greekAubergine,
  };
  const textStyle = {
    fontSize: 14,
    lineHeight: 20,
    letterSpacing: -0.3,
    color: palette.greekAubergine,
  };
  const ArrowContainer = styled.View`
    width: 100%;
    height: 24px;
    padding-left: 85px;
    justify-content: center;
  `;
  const Arrow = () => (
    <ArrowContainer>
      <Svg
        style={{ transform: [{ rotate: isOpened ? '90deg' : '-90deg' }] }}
        width={14}
        height={19}
        fill="none"
      >
        <Path
          d="M8 13 1.886 7.76a1 1 0 0 1 0-1.52L8 1"
          stroke={palette.greekAubergine}
          strokeWidth={2}
          strokeLinejoin="round"
        />
      </Svg>
    </ArrowContainer>
  );
  return (
    <View>
      <ModalDropdown
        options={data}
        style={dropDownstyles}
        defaultValue={`in ${selected || data[0]}`}
        textStyle={textStyle}
        renderRow={renderRow}
        isFullWidth={true}
        dropdownStyle={{ dropDownstyles }}
        animated
        renderRightComponent={() => <Arrow />}
        onDropdownWillShow={() => setIsOpened(true)}
        onDropdownWillHide={() => setIsOpened(false)}
      />
    </View>
  );
};
