import React, { FC } from 'react';
import { Image } from 'react-native';

import subsribe from '~/assets/images/cart/subscribe.png';
import { CartItem } from '~/molecules/cart';

import {
  CartContentBottomDescription,
  CartContentContainer,
  CartContentDescription,
  CartContentItems,
  cartContentItemsContentStyle,
  CartSubmitButton,
} from './styles';

export const CartContent: FC = () => {
  return (
    <CartContentContainer>
      <CartContentItems
        contentContainerStyle={cartContentItemsContentStyle}
        showsVerticalScrollIndicator={false}
      >
        <CartItem isFirst />
        <CartItem />
        <CartItem />
      </CartContentItems>
      <Image
        source={subsribe}
        // eslint-disable-next-line react-native/no-inline-styles
        style={{ width: '100%', height: 64, marginTop: 24 }}
        resizeMode="cover"
      />
      <CartContentDescription>
        Receive two bottles each month. Lorem ipsum dolor amet. Your card will
        be charged when product is shipped.
      </CartContentDescription>
      <CartSubmitButton title="Checkout  —  USD $120.00" />
      <CartContentBottomDescription>
        Shipping, taxes and promo codes {'\n'} calculated at checkout.
      </CartContentBottomDescription>
    </CartContentContainer>
  );
};
