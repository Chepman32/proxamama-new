import styled, { css } from '@emotion/native';

import { Button, Text } from '~/atoms/common';

export const CartContentContainer = styled.View`
  padding-top: 28px;
`;

export const CartContentDescription = styled(Text)`
  font-size: 12px;
  line-height: 18px;
  color: ${({ theme }) => theme.palette.greekAubergine60};
  margin-top: 18px;
`;

export const CartSubmitButton = styled(Button)`
  margin-top: 51px;
  height: 64px;
`;

export const CartContentBottomDescription = styled(Text)`
  font-size: 12px;
  line-height: 18px;
  color: ${({ theme }) => theme.palette.greekAubergine};
  text-align: center;
  margin-top: 16px;
`;

export const CartContentItems = styled.ScrollView`
  height: 300px;
`;

export const cartContentItemsContentStyle = css`
  padding-vertical: 10px;
`;
