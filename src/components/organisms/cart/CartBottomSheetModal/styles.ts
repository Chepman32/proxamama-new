import { initialWindowMetrics, Metrics } from 'react-native-safe-area-context';

import styled from '@emotion/native';
import { BottomSheetBackdrop } from '@gorhom/bottom-sheet';

import { Text } from '~/atoms/common';
export const CartBottomSheetModalContainer = styled.View`
  flex: 1;
  flew-direction: column;
  padding-top: 32px;
  padding-horizontal: 24px;
  padding-bottom: ${(
    initialWindowMetrics as Metrics
  ).insets.bottom.toString()}px;
`;
export const CartBottomSheetModalBackdrop = styled(BottomSheetBackdrop)`
  background-color: ${({ theme }) => theme.palette.blackSabbath};
`;

export const CartBottomSheetModalHeader = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const CartBottomSheetModalTitle = styled(Text)`
  font-size: 20px;
  font-weight: 500;
  color: ${({ theme }) => theme.palette.greekAubergine};
`;
