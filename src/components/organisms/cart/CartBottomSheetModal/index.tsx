import React, { forwardRef, useMemo } from 'react';

import { useTheme } from '@emotion/react';
import { BottomSheetModal, useBottomSheetModal } from '@gorhom/bottom-sheet';

import { RoundedClose } from '~/atoms/common';

import { CartContent } from '../CartContent';

import {
  CartBottomSheetModalBackdrop,
  CartBottomSheetModalContainer,
  CartBottomSheetModalHeader,
  CartBottomSheetModalTitle,
} from './styles';

interface Props {
  onDismiss: () => void;
}

export const CartBottomSheetModal = forwardRef<BottomSheetModal, Props>(
  ({ onDismiss }, ref) => {
    const snapPoints = useMemo(() => ['1%', '90%'], []);
    const theme = useTheme();

    const backgroundStyle = useMemo(
      () => ({
        backgroundColor: theme.palette.placeboOrange,
        borderRadius: 16,
      }),
      [theme.palette.placeboOrange],
    );

    const { dismiss } = useBottomSheetModal();

    return (
      <BottomSheetModal
        ref={ref}
        name="cart"
        snapPoints={snapPoints}
        backgroundStyle={backgroundStyle}
        handleComponent={null}
        backdropComponent={props => (
          <CartBottomSheetModalBackdrop
            {...props}
            pressBehavior={'close'}
            opacity={0.3}
          />
        )}
        onDismiss={onDismiss}
      >
        <CartBottomSheetModalContainer>
          <CartBottomSheetModalHeader>
            <CartBottomSheetModalTitle>Your cart</CartBottomSheetModalTitle>
            <RoundedClose onPress={() => dismiss('cart')} />
          </CartBottomSheetModalHeader>
          <CartContent />
        </CartBottomSheetModalContainer>
      </BottomSheetModal>
    );
  },
);
