import React, { FC } from 'react';

import { RowComponentContainer } from './styles';
interface IRowComponent {
  children: any;
}
export const RowComponent: FC<IRowComponent> = ({ children }) => {
  return <RowComponentContainer>{children}</RowComponentContainer>;
};
