import React from 'react';
import FlashMessage from 'react-native-flash-message';
import { QueryClientProvider } from 'react-query';
import { Provider } from 'react-redux';

import { ThemeProvider } from '@emotion/react';
import { RootNavigator } from 'navigation/RootNavigator';
import { queryClient } from 'queries';
import { store } from 'redux/store';

import { GlobalMutationSpinner } from '~/atoms/common';

import { theme } from './config';

export const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <QueryClientProvider client={queryClient}>
          <RootNavigator />
          <GlobalMutationSpinner />
        </QueryClientProvider>
      </Provider>
      <FlashMessage position="top" />
    </ThemeProvider>
  );
};
