import { showMessage } from 'react-native-flash-message';

import {
  GoogleSignin,
  NativeModuleError,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import { AxiosError, AxiosResponse } from 'axios';
import { StatusCodes } from 'http-status-codes';
import { queryClient } from 'queries';
import { store } from 'redux/store';

import { GOOGLE_WEB_CLIENT_ID } from '~/config';
import { MutationKeys, RETRY_COUNT } from '~/constants';

import {
  AuthFormErrorResponse,
  AuthFormResponse,
} from '../forms/loginSubmit/types';

import { transporter } from './transporter';

GoogleSignin.configure({
  webClientId: GOOGLE_WEB_CLIENT_ID,
  offlineAccess: true,
});

export const googleAuth = async () => {
  const checkResult = await executeGoogleService();
  queryClient.executeMutation<
    AxiosResponse<AuthFormResponse>,
    AxiosError<AuthFormErrorResponse>
  >({
    mutationKey: MutationKeys.GOOGLE_AUTH,
    mutationFn: () =>
      transporter({
        provider: 'google',
        token: `${checkResult?.idToken}`,
        ...checkResult?.user,
      }),
    onSuccess: ({ data }) => {
      store.dispatch.authFlow.signin(data.token);
    },
    onError: error => {
      if (error.response) {
        showMessage({
          type: 'danger',
          message: 'Google auth error',
          description: error.response.data.message,
        });
      }
    },
    retry: (failureCount, error) => {
      return (
        error.response?.status !== StatusCodes.FORBIDDEN &&
        failureCount < RETRY_COUNT
      );
    },
  });
};

export const executeGoogleService = async () => {
  try {
    await GoogleSignin.hasPlayServices();
    return GoogleSignin.signIn();
  } catch (e) {
    if ((e as NativeModuleError).code !== statusCodes.SIGN_IN_CANCELLED) {
      showMessage({
        type: 'danger',
        message: 'Google auth error',
      });
    }

    return Promise.reject(e);
  }
};
