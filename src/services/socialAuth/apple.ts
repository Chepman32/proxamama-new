import { showMessage } from 'react-native-flash-message';

import { appleAuth as appleAuthFromLib } from '@invertase/react-native-apple-authentication';
import { AxiosError, AxiosResponse } from 'axios';
import { StatusCodes } from 'http-status-codes';
import { queryClient } from 'queries';
import { store } from 'redux/store';

import { MutationKeys, RETRY_COUNT } from '~/constants';

import {
  AuthFormErrorResponse,
  AuthFormResponse,
} from '../forms/loginSubmit/types';

import { transporter } from './transporter';

const ERROR_MESSAGE = 'Apple auth error';

export const appleAuth = async () => {
  const { token, fullName } = await executeAppleService();
  queryClient.executeMutation<
    AxiosResponse<AuthFormResponse>,
    AxiosError<AuthFormErrorResponse>
  >({
    mutationKey: MutationKeys.APPLE_AUTH,
    mutationFn: () =>
      transporter({
        provider: 'apple',
        token: `${token}`,
        ...fullName,
      }),
    onSuccess: ({ data }) => {
      store.dispatch.authFlow.signin(data.token);
    },
    onError: error => {
      if (error.response) {
        showMessage({
          type: 'danger',
          message: ERROR_MESSAGE,
          description: error.response.data.message,
        });
      }
    },
    retry: (failureCount, error) => {
      return (
        error.response?.status !== StatusCodes.FORBIDDEN &&
        failureCount < RETRY_COUNT
      );
    },
  });
};

export const executeAppleService = async () => {
  try {
    const appleAuthRequestResponse = await appleAuthFromLib.performRequest({
      requestedOperation: appleAuthFromLib.Operation.LOGIN,
      requestedScopes: [
        appleAuthFromLib.Scope.EMAIL,
        appleAuthFromLib.Scope.FULL_NAME,
      ],
    });

    const credentialState = await appleAuthFromLib.getCredentialStateForUser(
      appleAuthRequestResponse.user,
    );

    if (credentialState === appleAuthFromLib.State.AUTHORIZED) {
      return {
        token: appleAuthRequestResponse.identityToken,
        email: appleAuthRequestResponse.email,
        fullName: appleAuthRequestResponse.fullName,
      };
    } else {
      throw new Error('Not Authorized');
    }
  } catch (e) {
    if ((e as { code: string }).code !== appleAuthFromLib.Error.CANCELED) {
      showMessage({
        type: 'danger',
        message: ERROR_MESSAGE,
      });
    }

    return Promise.reject(e);
  }
};
