import { AuthFormResponse } from '../forms/loginSubmit/types';
import { request } from '../request';

type TransporterParams = {
  provider: 'google' | 'facebook' | 'apple';
  token: string;
  [key: string]: unknown;
};

export const transporter = ({ provider, ...otherParams }: TransporterParams) =>
  request.post<AuthFormResponse>('users/social-auth', {
    provider_name: provider,
    ...otherParams,
  });
