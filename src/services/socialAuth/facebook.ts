import {
  AccessToken,
  LoginManager,
  Profile,
  Settings,
} from 'react-native-fbsdk-next';
import { showMessage } from 'react-native-flash-message';

import { AxiosError, AxiosResponse } from 'axios';
import { StatusCodes } from 'http-status-codes';
import { queryClient } from 'queries';
import { store } from 'redux/store';

import { FACEBOOK_APP_ID } from '~/config';
import { MutationKeys, RETRY_COUNT } from '~/constants';

import {
  AuthFormErrorResponse,
  AuthFormResponse,
} from '../forms/loginSubmit/types';

import { transporter } from './transporter';

Settings.setAppID(FACEBOOK_APP_ID);

const ERROR_MESSAGE = 'Facebook auth error';

export const facebookAuth = async () => {
  const checkResult = await executeFacebookService();
  queryClient.executeMutation<
    AxiosResponse<AuthFormResponse>,
    AxiosError<AuthFormErrorResponse>
  >({
    mutationKey: MutationKeys.FACEBOOK_AUTH,
    mutationFn: () =>
      transporter({
        provider: 'facebook',
        token: `${checkResult.token?.accessToken}`,
        ...checkResult?.profile,
      }),
    onSuccess: ({ data }) => {
      store.dispatch.authFlow.signin(data.token);
    },
    onError: error => {
      if (error.response) {
        showMessage({
          type: 'danger',
          message: ERROR_MESSAGE,
          description: error.response.data.message,
        });
      }
    },
    retry: (failureCount, error) => {
      return (
        error.response?.status !== StatusCodes.FORBIDDEN &&
        failureCount < RETRY_COUNT
      );
    },
  });
};

export const executeFacebookService = async () => {
  try {
    const loginResult = await LoginManager.logInWithPermissions([
      'public_profile',
      'email',
    ]);
    if (!loginResult.isCancelled) {
      const profile = await Profile.getCurrentProfile();
      const token = await AccessToken.getCurrentAccessToken();
      return { profile, token };
    } else {
      throw new Error(ERROR_MESSAGE);
    }
  } catch (e) {
    showMessage({
      type: 'danger',
      message: ERROR_MESSAGE,
    });

    return Promise.reject(e);
  }
};
