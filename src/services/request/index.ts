import axios from 'axios';

import { API_URL } from '~/config';

export const request = axios.create({
  baseURL: API_URL,
});

// request.interceptors.response.use(
//   response => response,
//   e => {
//     showMessage({
//       type: 'danger',
//       message: 'Network Error',
//     });

//     return Promise.reject(e);
//   },
// );
