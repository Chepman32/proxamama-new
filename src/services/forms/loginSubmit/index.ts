import { showMessage } from 'react-native-flash-message';

import { FormikHelpers } from 'formik';
import { store } from 'redux/store';

import { request } from '~/services';

import { LoginFormInitialValues, AuthFormResponse } from './types';

export const loginSubmit = (
  { email, password }: LoginFormInitialValues,
  { resetForm }: FormikHelpers<LoginFormInitialValues>,
) => {
  return request
    .post<AuthFormResponse>('users/login', {
      email,
      password,
    })
    .then(response => {
      store.dispatch.authFlow.signin(response.data.token);
    })
    .catch(e => {
      resetForm();
      if (e.response) {
        showMessage({
          message: 'Signin error',
          description: e.response.data.message,
          type: 'danger',
        });
      } else {
        return Promise.reject(e);
      }
    });
};
