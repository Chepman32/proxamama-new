export type LoginFormInitialValues = {
  email: string;
  password: string;
};

export type AuthFormResponse = {
  token: string;
};

export type AuthFormErrorResponse = {
  message: string;
};
