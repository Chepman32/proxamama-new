/* eslint-disable @typescript-eslint/no-unused-vars */
import { showMessage } from 'react-native-flash-message';

import { StackActions } from '@react-navigation/native';

import { navigationInstance } from '~/navigation';
import { request } from '~/services';

import { AuthFormResponse } from '../loginSubmit/types';

import { SignupFormInitialValues } from './types';

const SUCCESS_REGISTRATION_MESSAGE_DURATION = 6000;

export const signupSubmit = ({
  confirm_password,
  privacy_policy,
  ...formValues
}: SignupFormInitialValues) => {
  return request
    .post<AuthFormResponse>('users/register', formValues)
    .then(() => {
      navigationInstance.navigation?.dispatch(StackActions.replace('Login'));
      showMessage({
        type: 'success',
        message: 'Successful registration !',
        description: `A verification email has just been sent to ${formValues.email}, please check your inbox and follow the instructions to proceed.`,
        duration: SUCCESS_REGISTRATION_MESSAGE_DURATION,
      });
    })
    .catch(e => {
      if (e.response) {
        showMessage({
          type: 'danger',
          message: 'Signup error',
          description: e.response.data.message,
        });
      } else {
        return Promise.reject(e);
      }
    });
};
