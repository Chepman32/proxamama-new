export type SignupFormInitialValues = {
  provider_name: string;
  first_name: string;
  last_name: string;
  email: string;
  password: string;
  confirm_password: string;
  privacy_policy: boolean;
};
