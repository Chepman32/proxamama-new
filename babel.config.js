module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        alias: {
          '~/atoms': './src/components/atoms',
          '~/molecules': './src/components/molecules',
          '~/organisms': './src/components/organisms',
          '~/screens': './src/components/screens',
          '~/assets': './src/assets',
          '~/constants': './src/constants',
          '~/services': './src/services',
          '~/navigation': './src/navigation',
          '~/config': './src/config',
        },
        extensions: ['.js', '.ts', '.tsx'],
      },
    ],
    'react-native-reanimated/plugin',
  ],
};
