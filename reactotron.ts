import { NativeModules } from 'react-native';
import Reactotron from 'reactotron-react-native';
import { reactotronRedux } from 'reactotron-redux';

let scriptHostname;

const scriptURL = NativeModules.SourceCode.scriptURL;
scriptHostname = scriptURL.split('://')[1].split(':')[0];

let reactotronInstance = Reactotron.configure({
  host: scriptHostname,
})
  .useReactNative()
  .use(reactotronRedux());

if (__DEV__) {
  reactotronInstance.connect();
}

export { reactotronInstance };
